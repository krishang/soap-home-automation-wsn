#include <nrk.h>
#include <include.h>
#include <ulib.h>
#include <stdio.h>
#include <avr/sleep.h>
#include <hal.h>
#include <bmac.h>
#include <nrk_error.h>

#define NODE_ID			0
#define ROOM_ID			0
#define CRYPTO_KEY		190
#define BMAC_CHANNEL	19

uint8_t rx_buf[10];


nrk_task_type MULTI_SENSING_TASK;
NRK_STK sensors_task_stack[NRK_APP_STACKSIZE];
void read_sensors (void);

nrk_task_type TX_TASK;
NRK_STK tx_task_stack[NRK_APP_STACKSIZE];
void tx_task (void);

nrk_task_type RX_TASK;
NRK_STK rx_task_stack[NRK_APP_STACKSIZE];
void rx_task (void);

void nrk_create_taskset ();

struct beacon_packet
{
	uint8_t header[2];
	uint8_t ID;
	int encryption_key;
};

struct beacon_packet packet;


void init_node()
{
	packet.header[0] = 'I';
	packet.header[1] = 'D';
	packet.ID = NODE_ID;
	packet.encryption_key = CRYPTO_KEY;
}

int main ()
{
	nrk_setup_ports ();
	nrk_setup_uart (UART_BAUDRATE_115K2);

	nrk_init ();
	init_node();

	nrk_led_clr (0);
	nrk_led_clr (1);
	nrk_led_clr (2);
	nrk_led_clr (3);

	nrk_time_set (0, 0);

	bmac_task_config ();

	// init bmac on channel 19
	bmac_init(BMAC_CHANNEL);

	bmac_rx_pkt_set_buffer(rx_buf,RF_MAX_PAYLOAD_SIZE);

	nrk_create_taskset ();
	nrk_start ();

  return 0;
}

void tx_task (void)
{
	uint8_t j, i, val, len;

	uint16_t nrk_max_sleep_wakeup_time;
	nrk_sig_t tx_done_signal;
	nrk_sig_mask_t ret;
	//nrk_time_t end_time;
	
	printf ("tx_task PID=%d\r\n", nrk_get_pid ());
	
	// Wait until the tx_task starts up bmac
	while (!bmac_started ())
		nrk_wait_until_next_period ();

	// Get and register the tx_done_signal if you want to
	// do non-blocking transmits
	tx_done_signal = bmac_get_tx_done_signal ();
	nrk_signal_register (tx_done_signal);


	while (1) 
	{

		//nrk_time_get(&end_time);
		//printf("TX ID Task %d\r\n", end_time.secs);

		// Build a TX packet
		nrk_led_set (RED_LED);
		
		// This function shows how to transmit packets in a
		// non-blocking manner  
		val = bmac_tx_pkt_nonblocking(&packet, sizeof(packet));
		// This functions waits on the tx_done_signal
		ret = nrk_event_wait (SIG(tx_done_signal));

		/*
		// Just check to be sure signal is okay
		if(ret & SIG(tx_done_signal) == 0 ) 
			nrk_kprintf (PSTR ("TX done signal error\r\n"));
		else tx_data_ok=1;
		*/
		
		// Task gets control again after TX complete
		//printf("Return to Tx ID after wait\r\n");
		nrk_led_clr (RED_LED);	

		nrk_wait_until_next_period ();
	}
}


void nrk_create_taskset ()
{
  
  TX_TASK.task = tx_task;
  nrk_task_set_stk( &TX_TASK, tx_task_stack, NRK_APP_STACKSIZE);
  TX_TASK.prio = 2;
  TX_TASK.FirstActivation = TRUE;
  TX_TASK.Type = BASIC_TASK;
  TX_TASK.SchType = PREEMPTIVE;
  TX_TASK.period.secs = 3;
  TX_TASK.period.nano_secs = 0;
  TX_TASK.cpu_reserve.secs = 3;
  TX_TASK.cpu_reserve.nano_secs = 0;
  TX_TASK.offset.secs = 0;
  TX_TASK.offset.nano_secs = 0;
  nrk_activate_task (&TX_TASK);     


}
