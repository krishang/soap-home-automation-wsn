#include <SimbleeForMobile.h>
#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>


// include newlib printf float support (%f used in sprintf below)
asm(".global _printf_float");


#define PROFILE_SEMICOLONS  20
#define PROFILES_COMMAND    1

#define titleLeftMargin 120
#define titleTopMargin  40
#define segmentWidth    80
#define sliderWidth     100
#define leftMargin      20
#define home_bg         YELLOW
#define sliderSpace     35
#define sliderMargin    50
#define sliderLimit     255
#define bottomPaneHeight 50
#define controlBoxBound 185

#define FAN_INDEX       0
#define HUM_INDEX       1
#define R_INDEX         2
#define G_INDEX         3
#define B_INDEX         4

#define COMMAND_GET_PROFILE       0
#define COMMAND_GET_ALL_PROFILES  1
#define COMMAND_SEMICOLONS        12


Adafruit_MMA8451 mma = Adafruit_MMA8451();

int temperature, humidity, light_val, pir;
int beacon1_rssi,beacon2_rssi,beacon3_rssi;

int textBox;
int setupdone = 0;
char text_buf[10];

//UI Elements
char *beacons[]={"1","2","3"};

int currentScreen;
int  DBeacon_text_id;
int  dbeacon_id;

int  EBeacon_text_id;
int  ebeacon_id;

int  ABeacon_text_id;
int  abeacon_id;

int hh, mm, ss;

int fan_switch_id, humidifier_switch_id,Beacon_text_id, R_slider_id, G_slider_id, B_slider_id, priority_button_id;
int beacon1_text_id, beacon2_text_id, beacon3_text_id;
static int value[3][5], old_value[3][5];
static int curr_value[5];
int need_refresh, need_restore;
int R_slider_val_box, G_slider_val_box, B_slider_val_box;
int test_val = 54;

int add_profile_id, edit_profile_id, delete_profile_id;
int add_profile, edit_profile, delete_profile;
int selected_beacon_id;
int temperature_text_id,humidity_text_id,light_text_id,movement_text_id;
int test_id, sys_disabled;
float temp;

int add_id=0,edit_id=0,delete_id=0;
int added_beacon,edited_beacon,deleted_beacon;

bool priority_mode = false;
bool request_profiles = false;
bool send_profiles_to_pi = false;
bool send_present_profile_to_pi = false;
bool profiles_received = false;
bool profiles_request = true;
bool read_commands = false;
bool update_sensor_values = false;
bool paralyse_system = false;


void readCommandFromPi()
{
  if(!read_commands)
    return;
  if(Serial.available()>0)
  {
    String line= Serial.readString();
    int command = parseCommandData(line);
    switch(command)
    {
      case COMMAND_GET_ALL_PROFILES:  send_profiles_to_pi = true;
                                      break;
      case COMMAND_GET_PROFILE:       send_present_profile_to_pi = true;
                                      break;                                      
    }
    
  }    
}

void requestProfilesFromPi()
{
  Serial.println("PI;2;");
  profiles_request = false;
}

void getProfilesFromPi()
{
  //Serial.println("PI;2;");
  if(Serial.available()>0 )
  {
    String line= Serial.readString();
    if(parseProfileData(line) == 0)
    {
      Serial.println("ACK;"); 
      profiles_received = true;
      read_commands = true;
    }    
    
  }
}

void sendPresentProfile()
{
  if(send_profiles_to_pi)
    return;
  //SB;1;ID1;HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;
  char send_buffer[80];
  sprintf(send_buffer,"SB;%d;%d;%d;%d;%d;%d;",request_profiles,curr_value[FAN_INDEX],curr_value[HUM_INDEX],curr_value[R_INDEX],curr_value[G_INDEX],curr_value[B_INDEX]);
  Serial.println(send_buffer);
  request_profiles = false;
  send_present_profile_to_pi = false;
}

void sendProfiles()
{
  //SB;1;ID1;HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;
  char send_buffer[80];
  sprintf(send_buffer,"SB;1;0;%d;%d;%d;%d;%d;",value[0][FAN_INDEX],value[0][HUM_INDEX],value[0][R_INDEX],value[0][G_INDEX],value[0][B_INDEX]);
  sprintf(send_buffer,"%s1;%d;%d;%d;%d;%d;",send_buffer, value[1][FAN_INDEX],value[1][HUM_INDEX],value[1][R_INDEX],value[1][G_INDEX],value[1][B_INDEX]);
  sprintf(send_buffer,"%s2;%d;%d;%d;%d;%d;",send_buffer, value[2][FAN_INDEX],value[2][HUM_INDEX],value[2][R_INDEX],value[2][G_INDEX],value[2][B_INDEX]);
  Serial.println(send_buffer);

  send_profiles_to_pi = false;
}


void setup(void) 
{

  Serial.begin(115200);
  SimbleeForMobile.deviceName = "SOAP";
  SimbleeForMobile.advertisementData = "Room1";

  // use a shared cache
  SimbleeForMobile.domain = "testing.simblee.com";

  // Begin Simble UI
  SimbleeForMobile.begin();
  //value[0][1]=1;
  for(int i=0;i<5;i++)
  curr_value[i]=0;
  for(int i=0;i<3;i++){
    for(int j=0;j<5;j++)
    {
      value[i][j]=0;
    }
  }
  
}

void update_sensors()
{
  char temp[5];

    sprintf(temp,"%d",humidity);
    SimbleeForMobile.updateText(humidity_text_id,temp);
    if(pir >= 800)
    {
      sprintf(temp,"%d:%d:%d",hh,mm,ss);
      SimbleeForMobile.updateText(movement_text_id,temp);
    }
    sprintf(temp,"%d",temperature);
    SimbleeForMobile.updateText(temperature_text_id,temp);
    sprintf(temp,"%d",light_val);
    SimbleeForMobile.updateText(light_text_id,temp); 

    
    if(paralyse_system)
      SimbleeForMobile.updateText(sys_disabled,"System Disabled!"); 
    else
      SimbleeForMobile.updateText(sys_disabled,""); 
    
    SimbleeForMobile.setVisible(beacon1_text_id, beacon1_rssi>0?true:false);
    SimbleeForMobile.setVisible(beacon2_text_id, beacon2_rssi>0?true:false);
    SimbleeForMobile.setVisible(beacon3_text_id, beacon3_rssi>0?true:false);

    update_sensor_values = false;

  
}

void loop() 
{

  if(setupdone && need_refresh && SimbleeForMobile.updatable)
    updateFields();
  if(need_restore && SimbleeForMobile.updatable)
  {
    //Serial.println("Restoring values");
    restoreValues(need_restore-1);
  }

  if(!profiles_received)
  {
    getProfilesFromPi();
  }
  
  if(send_profiles_to_pi)
  {
    sendProfiles();
  }
  if(profiles_request)
  {
    requestProfilesFromPi();
  }  
  if(send_present_profile_to_pi)
  {
    sendPresentProfile();
  }

  readCommandFromPi();
  if(update_sensor_values)
    update_sensors();
    
  SimbleeForMobile.process();
}

void ui_event(event_t &event)
{
  char buffer[40];
  need_refresh = 1;
  if(paralyse_system)
    return;
  if (event.id == fan_switch_id) 
  {
      curr_value[FAN_INDEX] = event.value;
  }
  else if (event.id == humidifier_switch_id) 
  {
      
      curr_value[HUM_INDEX] = event.value;
  }  
  else if (event.id == R_slider_id) 
  {
      
      curr_value[R_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value); 
      
  }  
  else if (event.id == G_slider_id) 
  {
      
      curr_value[G_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value);
      
  }    
  else if (event.id == B_slider_id) 
  {
      
      curr_value[B_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value); 
        
  }    
  else if(event.id == beacon1_text_id)
  {
    curr_value[FAN_INDEX] = value[0][FAN_INDEX];
    curr_value[HUM_INDEX] = value[0][HUM_INDEX];
    curr_value[R_INDEX] = value[0][R_INDEX];
    curr_value[G_INDEX] = value[0][G_INDEX];
    curr_value[B_INDEX] = value[0][B_INDEX];
    need_restore = 1; 
    selected_beacon_id = beacon1_text_id;
    abeacon_id = 0;
    //SimbleeForMobile.updateValue(ABeacon_text_id,0);
  }
  else if(event.id == beacon2_text_id)
  {
    curr_value[FAN_INDEX] = value[1][FAN_INDEX];
    curr_value[HUM_INDEX] = value[1][HUM_INDEX];
    curr_value[R_INDEX] = value[1][R_INDEX];
    curr_value[G_INDEX] = value[1][G_INDEX];
    curr_value[B_INDEX] = value[1][B_INDEX];
    need_restore = 2; 
    selected_beacon_id = beacon2_text_id;  
    abeacon_id = 1;
    //SimbleeForMobile.updateValue(ABeacon_text_id,1);
  }  
  else if(event.id == beacon3_text_id)
  {
    curr_value[FAN_INDEX] = value[2][FAN_INDEX];
    curr_value[HUM_INDEX] = value[2][HUM_INDEX];
    curr_value[R_INDEX] = value[2][R_INDEX];
    curr_value[G_INDEX] = value[2][G_INDEX];
    curr_value[B_INDEX] = value[2][B_INDEX];
    need_restore = 3; 
    selected_beacon_id = beacon3_text_id;
    abeacon_id = 2;
    //SimbleeForMobile.updateValue(ABeacon_text_id,2);

  }  
   /*
  else if (event.id == DBeacon_text_id) 
  {
      dbeacon_id=event.value;
      //Serial.print("beacon selected to delete is:");
      //Serial.println(dbeacon_id);
  } */
  else if (event.id == delete_profile_id) 
  {
      
      //Serial.println(beacons[dbeacon_id]);     
      for(int i=0;i<5;i++)
      {
        value[dbeacon_id][i]=0;
      }
      need_restore=dbeacon_id+1;
      request_profiles = true;
      
  } 
/*
  else if (event.id == ABeacon_text_id) 
  {
      abeacon_id=event.value;
      //Serial.print("beacon selected to add is:");
      //Serial.println(abeacon_id);
  }*/
  else if (event.id == add_profile_id) 
  {
      for(int i=0;i<5;i++)
      {
        value[abeacon_id][i]=curr_value[i];
        //Serial.println(value[abeacon_id][i]);     
      }
      request_profiles = true;
      
      
  } 
  
  /*
  else if(event.id == edit_profile_id)
  {

    if(selected_beacon_id==beacon1_text_id)
    {
      for(int i=0;i<5;i++)
      {
        value[0][i]=curr_value[i];
      }
    }
    
    if(selected_beacon_id==beacon2_text_id)
    {
      for(int i=0;i<5;i++)
      {
        value[1][i]=curr_value[i];
      }
    }
    
    if(selected_beacon_id==beacon3_text_id)
    {
      for(int i=0;i<5;i++)
      {
        value[2][i]=curr_value[i];
      }
    }
  }
*/
   

  else if(event.id == priority_button_id)
  {
    priority_mode = event.value?true:false;
    need_restore =1; 
  }
  
  else
  {
    need_refresh = 0;
  }
}

void updateBeaconButtons()
{
    SimbleeForMobile.updateColor(beacon1_text_id, (selected_beacon_id == beacon1_text_id)? BLUE:GRAY);
    SimbleeForMobile.updateColor(beacon2_text_id, (selected_beacon_id == beacon2_text_id)? BLUE:GRAY);
    SimbleeForMobile.updateColor(beacon3_text_id, (selected_beacon_id == beacon3_text_id)? BLUE:GRAY);  
    SimbleeForMobile.updateValue(priority_button_id, (priority_mode)? true:false);  
}

void updateFields()
{
      char buffer[40];
      sprintf(text_buf,"%d",curr_value[R_INDEX]);
      SimbleeForMobile.updateText(R_slider_val_box,text_buf); 

      sprintf(text_buf,"%d",curr_value[G_INDEX]);
      SimbleeForMobile.updateText(G_slider_val_box,text_buf);     

      sprintf(text_buf,"%d",curr_value[B_INDEX]);
      SimbleeForMobile.updateText(B_slider_val_box,text_buf);  

      sprintf(buffer, "F %d ; H %d R %d G %d B %d", curr_value[FAN_INDEX],curr_value[HUM_INDEX], curr_value[R_INDEX], curr_value[G_INDEX], curr_value[B_INDEX]);
      //Serial.println(buffer);    
      need_refresh = 0;   
}

void restoreValues(int index)
{
  //Serial.println(value[index][FAN_INDEX]);
  //Serial.println(value[index][HUM_INDEX]);
  //Serial.println(value[index][R_INDEX]);
  //Serial.println(value[index][G_INDEX]); 
  //Serial.println(value[index][B_INDEX]);
  SimbleeForMobile.updateValue(fan_switch_id,value[index][FAN_INDEX]);
  SimbleeForMobile.updateValue(humidifier_switch_id,value[index][HUM_INDEX]);
  SimbleeForMobile.updateValue(R_slider_id,value[index][R_INDEX]);
  SimbleeForMobile.updateValue(G_slider_id,value[index][G_INDEX]);
  SimbleeForMobile.updateValue(B_slider_id,value[index][B_INDEX]);

  
  updateBeaconButtons();
  need_restore = 0;
}

void SimbleeForMobile_onConnect()
{
  char buffer[40];
    sprintf(buffer, "Starting F %d ; H %d R %d G %d B %d", curr_value[FAN_INDEX],curr_value[HUM_INDEX], curr_value[R_INDEX], curr_value[G_INDEX], curr_value[B_INDEX]);
    //Serial.println(buffer);    
}

void setup_home_screen()
{
  setupdone=0;

  unsigned long rect_color = 0x3f8f8f00;
  int pass = 0;
  SimbleeForMobile.beginScreen(home_bg);

  //Serial.println("Starting Home Screen");
  int x_margin = 0, y_margin = 0;

  x_margin = titleLeftMargin;
  y_margin = titleTopMargin;
  SimbleeForMobile.drawText(0 , 0, "SOAP", WHITE, 5);

  
  int home_heading = SimbleeForMobile.drawText(x_margin , y_margin, "SOAP", BLACK, 35);
  int room_id_text = SimbleeForMobile.drawText(x_margin+10 , y_margin+35, "Room 1", BLACK, 20);
  
  y_margin += 80;
  x_margin = leftMargin-10;  
  int custom_control_rect = SimbleeForMobile.drawRect(leftMargin - 5, y_margin, controlBoxBound, 220, rect_color);

  y_margin += 5;
  x_margin = leftMargin+5;
  SimbleeForMobile.drawText(x_margin , y_margin, "FAN", BLACK, 15);

  x_margin = controlBoxBound - 80;
  SimbleeForMobile.drawText(x_margin , y_margin, "HUMIDIFIER", BLACK, 15);

  y_margin += 20;
  x_margin = leftMargin;
  fan_switch_id = SimbleeForMobile.drawSwitch(x_margin, y_margin, rect_color);
  //Serial.println("fan_switch_value");
  //Serial.println(fan_switch_id);
  SimbleeForMobile.updateValue(fan_switch_id,curr_value[FAN_INDEX]);

  x_margin = controlBoxBound - 45;
  humidifier_switch_id = SimbleeForMobile.drawSwitch(x_margin, y_margin, rect_color);
  SimbleeForMobile.updateValue(humidifier_switch_id,curr_value[HUM_INDEX]);

  

  y_margin += 40;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin, y_margin, "LED COLOR", BLACK, 15);
  //SimbleeForMobile.setEvents(humidifier_switch_id, EVENT_RELEASE);

  
  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "RED", RED, 15);
  x_margin += sliderMargin;
  R_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, RED);
  SimbleeForMobile.updateValue(R_slider_id,curr_value[R_INDEX]);

  x_margin += sliderWidth + 5;
  R_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "0", RED, 15);
  

  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "GREEN", GREEN, 15);
  x_margin += sliderMargin;
  G_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, GREEN);
  SimbleeForMobile.updateValue(G_slider_id,curr_value[G_INDEX]);

  x_margin += sliderWidth + 5;
  G_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "0", GREEN, 15);  
  

  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "BLUE", BLUE, 15);
  x_margin += sliderMargin;
  B_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, BLUE);
  SimbleeForMobile.updateValue(B_slider_id,curr_value[B_INDEX]);

  x_margin += sliderWidth + 5;
  B_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "0", BLUE, 15);   
  


  x_margin = controlBoxBound + leftMargin ;
  y_margin = titleTopMargin + 80;
  int beacons_rect = SimbleeForMobile.drawRect(x_margin, y_margin, SimbleeForMobile.screenWidth - x_margin - 10, 220, rect_color);

  

  x_margin += 10;
  y_margin += 10;
  SimbleeForMobile.drawText(x_margin , y_margin, "PRESETS", BLACK, 20);
  
  y_margin += 40;
  beacon1_text_id = SimbleeForMobile.drawButton(x_margin , y_margin, 60, "Beacon1", GRAY, TEXT_TYPE);  
  SimbleeForMobile.setEvents(beacon1_text_id, EVENT_RELEASE);

  y_margin += 40;  
  beacon2_text_id = SimbleeForMobile.drawButton(x_margin , y_margin,60, "Beacon2", GRAY, TEXT_TYPE); 
  SimbleeForMobile.setEvents(beacon2_text_id, EVENT_RELEASE);

  y_margin += 40;
  beacon3_text_id = SimbleeForMobile.drawButton(x_margin , y_margin,60, "Beacon3", GRAY, TEXT_TYPE); 
  SimbleeForMobile.setEvents(beacon3_text_id, EVENT_RELEASE);

  x_margin += 20;
  y_margin += 45;
  SimbleeForMobile.drawText(x_margin , y_margin, "Priority Mode", BLACK, 12);
  y_margin += 15;
  x_margin += 25;
  priority_button_id = SimbleeForMobile.drawSwitch(x_margin , y_margin, rect_color); 
  SimbleeForMobile.setEvents(priority_button_id, EVENT_RELEASE);  


  y_margin = titleTopMargin + 80 + 220 + 10;
  x_margin = leftMargin - 5;
  SimbleeForMobile.drawRect(x_margin, y_margin, (SimbleeForMobile.screenWidth * 30)/100, SimbleeForMobile.screenHeight - y_margin - 60, rect_color);

  //x_margin += 10;
  //y_margin += 10;

  //SimbleeForMobile.drawText(x_margin+10, y_margin+100, "2245", BLACK, 15);
  temperature_text_id = SimbleeForMobile.drawText(x_margin+20, y_margin+50, "25", BLACK, 35);
  SimbleeForMobile.drawText(x_margin+60, y_margin+50, "\xc2\xb0" "C", BLACK, 25);

  
  
 
   
  
  y_margin = titleTopMargin + 80 + 220 + 10;
  x_margin = leftMargin + (SimbleeForMobile.screenWidth * 30)/100;
  SimbleeForMobile.drawRect(x_margin, y_margin, SimbleeForMobile.screenWidth - x_margin - 10 , SimbleeForMobile.screenHeight - y_margin - 60, rect_color); 


  humidity_text_id = SimbleeForMobile.drawText(x_margin+100, y_margin+40, "75%", BLACK, 15);
  light_text_id = SimbleeForMobile.drawText(x_margin+100, y_margin+70, "85%", BLACK, 15);
  SimbleeForMobile.drawText(x_margin+10, y_margin+100, "Last Movement Detected: ", BLACK, 15);
  movement_text_id = SimbleeForMobile.drawText(x_margin+70, y_margin+130, "never", BLACK, 15);

  sys_disabled = SimbleeForMobile.drawText(x_margin+10, y_margin+10, "", BLACK, 25);

  //SimbleeForMobile.drawText(x_margin+10, y_margin+10, "T :", BLACK, 15);
  SimbleeForMobile.drawText(x_margin+10, y_margin+40, "Humidity(%): ", BLACK, 15);
  SimbleeForMobile.drawText(x_margin+10, y_margin+70, "Light(%): ", BLACK, 15);
  
  //x_margin += 10;
  //y_margin += 10;
  /*
  if(beacons!=NULL)
  {
  SimbleeForMobile.drawText(x_margin+10 , y_margin+30, "ADD_BEACON", BLACK, 15);

  ABeacon_text_id = SimbleeForMobile.drawSegment(x_margin+130, y_margin+30,  60,  beacons, 3,BLUE);
  }


  if(beacons!=NULL)
  {
  SimbleeForMobile.drawText(x_margin+10 , y_margin+80, "DEL_BEACON", BLACK, 15);

  DBeacon_text_id = SimbleeForMobile.drawSegment(x_margin+130, y_margin+80,  60,  beacons, 3,BLUE);
  }
  */
  y_margin += 40;
  
  y_margin += 60;
   
   

  
  y_margin = SimbleeForMobile.screenHeight - bottomPaneHeight;

  int unit_width = SimbleeForMobile.screenWidth/2 - 5;
  
  SimbleeForMobile.drawRect(0,y_margin,unit_width, bottomPaneHeight,rect_color);
  add_profile_id = SimbleeForMobile.drawButton(0 , y_margin + 5,unit_width, "Save Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(add_profile_id, EVENT_RELEASE);
/*
  SimbleeForMobile.drawRect(unit_width+5,y_margin,unit_width+7,bottomPaneHeight,rect_color);
  edit_profile_id = SimbleeForMobile.drawButton(unit_width+5,y_margin + 5,unit_width+7, "Edit Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(edit_profile_id, EVENT_RELEASE);  
*/
  SimbleeForMobile.drawRect(SimbleeForMobile.screenWidth - unit_width,y_margin,unit_width,bottomPaneHeight,rect_color);  
  delete_profile_id = SimbleeForMobile.drawButton(SimbleeForMobile.screenWidth - unit_width,y_margin + 5,unit_width, "Delete Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(delete_profile_id, EVENT_RELEASE);  

  SimbleeForMobile.setEvents(add_profile_id, EVENT_RELEASE);
//  SimbleeForMobile.setEvents(edit_profile_id, EVENT_RELEASE);
  SimbleeForMobile.setEvents(delete_profile_id, EVENT_RELEASE);
  
  SimbleeForMobile.endScreen();
  need_restore = 1;
  setupdone = 1;  
}



void ui()
{

  setup_home_screen();

}













