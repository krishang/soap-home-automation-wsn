int parseCommandData(String line)
{
  int comm = -1;
  String header = parse_curr(line,';');
  if(header!="PI")
  {
    return -1;
  }   
  if(count_char(line,';') != COMMAND_SEMICOLONS)
  {
    return -1;
  }  
  line = parse_next(line,';');
  comm = parse_curr(line,';').toInt();

//  sprintf(simblee_buffer,"%s%d;%d;%d;%d;%d;%d;%d;%d;%d",simblee_buffer,temp,hum,light_val,pir,rssi_1,rssi_2,rssi_3,tm.tm_hour, tm.tm_min);
  //PI;0/1/2;Temp;Hum;Light;PIR;b1;b2;b3;hh;mm;
  line = parse_next(line,';');
  temperature = parse_curr(line,';').toInt();

  line = parse_next(line,';');
  humidity = parse_curr(line,';').toInt();  

  line = parse_next(line,';');
  light_val = parse_curr(line,';').toInt();  

  line = parse_next(line,';');
  pir = parse_curr(line,';').toInt();  
  
  line = parse_next(line,';');
  beacon1_rssi = parse_curr(line,';').toInt();  

  line = parse_next(line,';');
  beacon2_rssi = parse_curr(line,';').toInt();    

  line = parse_next(line,';');
  beacon3_rssi = parse_curr(line,';').toInt();    

  line = parse_next(line,';');
  hh = parse_curr(line,';').toInt();  

  line = parse_next(line,';');
  mm = parse_curr(line,';').toInt();    

  line = parse_next(line,';');
  ss = parse_curr(line,';').toInt();    

  paralyse_system = (hh>=12)?false:true;
  
  update_sensor_values = true;
  return comm;
}

int parseProfileData(String line)
{

  String header = parse_curr(line,';');
  if(header!="SB")
  {
    return -1;
  }  
  if(count_char(line,';') != PROFILE_SEMICOLONS)
  {
    return -1;
  }
  line = parse_next(line,';');
  int cmd_id = parse_curr(line,';').toInt();
  if(cmd_id != PROFILES_COMMAND)
  {
    return -1;
  }
  line = parse_next(line,';');

  //HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;
  for(int i = 0;i<3;i++)
  {
    int id = parse_curr(line,';').toInt();
    line = parse_next(line,';');
    int hum = parse_curr(line,';').toInt();
    line = parse_next(line,';');
    int fan = parse_curr(line,';').toInt();
    line = parse_next(line,';');
    int r = parse_curr(line,';').toInt();
    line = parse_next(line,';');
    int g = parse_curr(line,';').toInt();
    line = parse_next(line,';');
    int b = parse_curr(line,';').toInt();
    line = parse_next(line,';');

    value[i][FAN_INDEX] = fan;
    value[i][HUM_INDEX] = hum;
    value[i][R_INDEX] = r;
    value[i][G_INDEX] = g;
    value[i][B_INDEX] = b;
    
  }
  
  


  //SB;1;ID1;HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;


  return 0;
}
/*
int serial_parse(String line)
{
    
  String s_1=parse_next(line,':');
  
    r[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(r[i]);
    g[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(g[i]);
    b[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(b[i]);
  for(int i = 1; i<4;i++)
  {
    r[i] = r[0];
    g[i] = g[0];
    b[i] = b[0];      
  }

  fan=parse_curr(s_1,':').toInt();
  //Serial.println(fan);
  s_1=parse_next(s_1,':');
  humid=parse_next(s_1,':').toInt();
  //Serial.println(humid);
  return 0;
}
*/
String parse_curr(String s, char delimiter)
{
  int del_pos=s.indexOf(delimiter);

  String s_0=s.substring(0,del_pos);
  return s_0;
}

String parse_next(String s, char delimiter)
{
  int del_pos=s.indexOf(delimiter);

  String s_0=s.substring(del_pos+1);
  return s_0;
}


int count_char(String str, char c)
{
  int i = 0, count = 0;
  unsigned int len = str.length();
  for(i = 0; i<len;i++)
  {
    if(str[i] == c)
      count++;
  } 
  return count;
}

