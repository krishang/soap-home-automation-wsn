#include <SimbleeForMobile.h>
#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
// include newlib printf float support (%f used in sprintf below)
asm(".global _printf_float");



#define titleLeftMargin 120
#define titleTopMargin  40
#define segmentWidth    80
#define sliderWidth     100
#define leftMargin      20
#define home_bg         YELLOW
#define sliderSpace     35
#define sliderMargin    50
#define sliderLimit     255
#define bottomPaneHeight 50
#define controlBoxBound 185

#define FAN_INDEX       0
#define HUM_INDEX       1
#define R_INDEX         2
#define G_INDEX         3
#define B_INDEX         4


Adafruit_MMA8451 mma = Adafruit_MMA8451();


int textBox;
int setupdone = 0;
char text_buf[10];

//UI Elements
int fan_switch_id, humidifier_switch_id, R_slider_id, G_slider_id, B_slider_id, priority_button_id;
int beacon1_text_id, beacon2_text_id, beacon3_text_id;
static int value[5], old_value[5];
bool need_refresh, need_restore;
int R_slider_val_box, G_slider_val_box, B_slider_val_box;
int test_val = 54;

int add_profile_id, edit_profile_id, delete_profile_id;
int selected_beacon_id;
int temperature_text_id;
int test_id;
float temp;

bool priority_mode = false;


void setup(void) 
{

  Serial.begin(115200);
  SimbleeForMobile.deviceName = "SOAP";
  SimbleeForMobile.advertisementData = "Room1";

  // use a shared cache
  SimbleeForMobile.domain = "testing.simblee.com";

  // Begin Simble UI
  SimbleeForMobile.begin();
  
}


void loop() 
{

  if(setupdone && need_refresh)
    updateFields();
  if(need_restore)
    restoreValues();
/*
  if (SimbleeForMobile.updatable)
  {


    temp = Simblee_temperature(CELSIUS);
  
    // requires newlib printf float support
    char buf[16];
    sprintf(buf, "%d", (int)temp);  
    SimbleeForMobile.updateText(temperature_text_id, buf);    
  }
  
   */ 
    
  SimbleeForMobile.process();
}

void ui_event(event_t &event)
{
  need_refresh = true;
  if (event.id == fan_switch_id) 
  {
      old_value[FAN_INDEX] = value[FAN_INDEX];
      value[FAN_INDEX] = event.value;
  }
  else if (event.id == humidifier_switch_id) 
  {
      old_value[HUM_INDEX] = value[HUM_INDEX];
      value[HUM_INDEX] = event.value;
  }  
  else if (event.id == R_slider_id) 
  {
      old_value[R_INDEX] = value[R_INDEX];
      value[R_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value); 
  }  
  else if (event.id == G_slider_id) 
  {
      old_value[G_INDEX] = value[G_INDEX];
      value[G_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value); 
  }    
  else if (event.id == B_slider_id) 
  {
      old_value[B_INDEX] = value[B_INDEX];
      value[B_INDEX] = (event.value < 0)?0: ((event.value >sliderLimit)?sliderLimit:event.value);     
  }    
  else if(event.id == beacon1_text_id)
  {
    value[FAN_INDEX] = 1;
    value[HUM_INDEX] = 0;
    value[R_INDEX] = 18;
    value[G_INDEX] = 59;
    value[B_INDEX] = 65;
    need_restore = true; 
    selected_beacon_id = beacon1_text_id;
  }
  else if(event.id == beacon2_text_id)
  {
    value[FAN_INDEX] = 0;
    value[HUM_INDEX] = 1;
    value[R_INDEX] = 36;
    value[G_INDEX] = 72;
    value[B_INDEX] = 10;    
    need_restore = true; 
    selected_beacon_id = beacon2_text_id;  
  }  
  else if(event.id == beacon3_text_id)
  {
    value[FAN_INDEX] = 1;
    value[HUM_INDEX] = 1;
    value[R_INDEX] = 75;
    value[G_INDEX] = 13;
    value[B_INDEX] = 54;   
    need_restore = true; 
    selected_beacon_id = beacon3_text_id;

  }  
  else if(event.id == add_profile_id)
  {
    
  }

  else if(event.id == edit_profile_id)
  {
    
  }

  else if(event.id == delete_profile_id)
  {
    
  }    

  else if(event.id == priority_button_id)
  {
    priority_mode = event.value?true:false;
    need_restore = true; 
  }
  
  else
  {
    need_refresh = false;
  }
}

void updateBeaconButtons()
{
    SimbleeForMobile.updateColor(beacon1_text_id, (selected_beacon_id == beacon1_text_id)? BLUE:GRAY);
    SimbleeForMobile.updateColor(beacon2_text_id, (selected_beacon_id == beacon2_text_id)? BLUE:GRAY);
    SimbleeForMobile.updateColor(beacon3_text_id, (selected_beacon_id == beacon3_text_id)? BLUE:GRAY);  
    SimbleeForMobile.updateValue(priority_button_id, (priority_mode)? true:false);  
}

void updateFields()
{
      char buffer[40];
      sprintf(text_buf,"%d",value[R_INDEX]);
      SimbleeForMobile.updateText(R_slider_val_box,text_buf); 

      sprintf(text_buf,"%d",value[G_INDEX]);
      SimbleeForMobile.updateText(G_slider_val_box,text_buf);     

      sprintf(text_buf,"%d",value[B_INDEX]);
      SimbleeForMobile.updateText(B_slider_val_box,text_buf);  

      sprintf(buffer, "F %d ; H %d R %d G %d B %d", value[FAN_INDEX],value[HUM_INDEX], value[R_INDEX], value[G_INDEX], value[B_INDEX]);
      Serial.println(buffer);    
      need_refresh = false;   
}

void restoreValues()
{
  SimbleeForMobile.updateValue(fan_switch_id,value[FAN_INDEX]);
  SimbleeForMobile.updateValue(humidifier_switch_id,value[HUM_INDEX]);
  SimbleeForMobile.updateValue(R_slider_id,value[R_INDEX]);
  SimbleeForMobile.updateValue(G_slider_id,value[G_INDEX]);
  SimbleeForMobile.updateValue(B_slider_id,value[B_INDEX]);
  updateBeaconButtons();
  need_restore = false;
}

void SimbleeForMobile_onConnect()
{
  char buffer[40];
    sprintf(buffer, "Starting F %d ; H %d R %d G %d B %d", value[FAN_INDEX],value[HUM_INDEX], value[R_INDEX], value[G_INDEX], value[B_INDEX]);
    Serial.println(buffer);    
}

void setup_home_screen()
{

  unsigned long rect_color = 0x3f8f8f00;
  int pass = 0;
  SimbleeForMobile.beginScreen(home_bg);

  Serial.println("Starting Home Screen");
  int x_margin = 0, y_margin = 0;

  x_margin = titleLeftMargin;
  y_margin = titleTopMargin;
  SimbleeForMobile.drawText(0 , 0, "SOAP", WHITE, 5);

  
  int home_heading = SimbleeForMobile.drawText(x_margin , y_margin, "SOAP", BLACK, 35);
  int room_id_text = SimbleeForMobile.drawText(x_margin+10 , y_margin+35, "Room 1", BLACK, 20);
  
  y_margin += 80;
  x_margin = leftMargin-10;  
  int custom_control_rect = SimbleeForMobile.drawRect(leftMargin - 5, y_margin, controlBoxBound, 220, rect_color);

  y_margin += 5;
  x_margin = leftMargin+5;
  SimbleeForMobile.drawText(x_margin , y_margin, "FAN", BLACK, 15);

  x_margin = controlBoxBound - 80;
  SimbleeForMobile.drawText(x_margin , y_margin, "HUMIDIFIER", BLACK, 15);

  y_margin += 20;
  x_margin = leftMargin;
  fan_switch_id = SimbleeForMobile.drawSwitch(x_margin, y_margin, rect_color);
  SimbleeForMobile.updateValue(fan_switch_id,value[FAN_INDEX]);

  x_margin = controlBoxBound - 45;
  humidifier_switch_id = SimbleeForMobile.drawSwitch(x_margin, y_margin, rect_color);
  SimbleeForMobile.updateValue(humidifier_switch_id,value[HUM_INDEX]);

  

  y_margin += 40;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin, y_margin, "LED COLOR", BLACK, 15);
  //SimbleeForMobile.setEvents(humidifier_switch_id, EVENT_RELEASE);

  
  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "RED", RED, 15);
  x_margin += sliderMargin;
  R_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, RED);


  x_margin += sliderWidth + 5;
  R_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "255", RED, 15);
  

  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "GREEN", GREEN, 15);
  x_margin += sliderMargin;
  G_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, GREEN);

  x_margin += sliderWidth + 5;
  G_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "255", GREEN, 15);  
  

  y_margin += sliderSpace;
  x_margin = leftMargin;
  SimbleeForMobile.drawText(x_margin , y_margin, "BLUE", BLUE, 15);
  x_margin += sliderMargin;
  B_slider_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, sliderLimit, BLUE);
  SimbleeForMobile.updateValue(R_slider_id,value[B_INDEX]);

  x_margin += sliderWidth + 5;
  B_slider_val_box = SimbleeForMobile.drawText(x_margin , y_margin, "", BLUE, 15);   
  

 /*

  x_margin = 20;
  y_margin += 60;
  
  x_margin += 100;
  test_id = SimbleeForMobile.drawSlider(x_margin, y_margin, sliderWidth, 0, 100, YELLOW);  
  SimbleeForMobile.updateValue(test_id,test_val);
*/


  x_margin = controlBoxBound + leftMargin ;
  y_margin = titleTopMargin + 80;
  int beacons_rect = SimbleeForMobile.drawRect(x_margin, y_margin, SimbleeForMobile.screenWidth - x_margin - 10, 220, rect_color);

  

  x_margin += 10;
  y_margin += 10;
  SimbleeForMobile.drawText(x_margin , y_margin, "PRESETS", BLACK, 20);
  
  y_margin += 40;
  beacon1_text_id = SimbleeForMobile.drawButton(x_margin , y_margin, 60, "Beacon1", GRAY, TEXT_TYPE);  
  SimbleeForMobile.setEvents(beacon1_text_id, EVENT_RELEASE);

  y_margin += 40;  
  beacon2_text_id = SimbleeForMobile.drawButton(x_margin , y_margin,60, "Beacon2", GRAY, TEXT_TYPE); 
  SimbleeForMobile.setEvents(beacon2_text_id, EVENT_RELEASE);

  y_margin += 40;
  beacon3_text_id = SimbleeForMobile.drawButton(x_margin , y_margin,60, "Beacon3", GRAY, TEXT_TYPE); 
  SimbleeForMobile.setEvents(beacon3_text_id, EVENT_RELEASE);

  x_margin += 20;
  y_margin += 45;
  SimbleeForMobile.drawText(x_margin , y_margin, "Priority Mode", BLACK, 12);
  y_margin += 15;
  x_margin += 25;
  priority_button_id = SimbleeForMobile.drawSwitch(x_margin , y_margin, rect_color); 
  SimbleeForMobile.setEvents(priority_button_id, EVENT_RELEASE);  


  y_margin = titleTopMargin + 80 + 220 + 10;
  x_margin = leftMargin - 5;
  SimbleeForMobile.drawRect(x_margin, y_margin, (SimbleeForMobile.screenWidth * 30)/100, SimbleeForMobile.screenHeight - y_margin - 60, rect_color);

  x_margin += 10;
  y_margin += 60;
  temperature_text_id = SimbleeForMobile.drawText(x_margin, y_margin, "25", BLACK, 30);
  x_margin += 40;
  SimbleeForMobile.drawText(x_margin, y_margin, "\xc2\xb0" "C", BLACK, 30);
  
  

  y_margin = titleTopMargin + 80 + 220 + 10;
  x_margin = leftMargin + (SimbleeForMobile.screenWidth * 30)/100;
  SimbleeForMobile.drawRect(x_margin, y_margin, SimbleeForMobile.screenWidth - x_margin - 10 , SimbleeForMobile.screenHeight - y_margin - 60, rect_color); 
  x_margin += 10;
  y_margin += 30;
  SimbleeForMobile.drawText(x_margin, y_margin, "Humidity : 75%", BLACK, 20);
  y_margin += 40;
  SimbleeForMobile.drawText(x_margin, y_margin, "Light : 85%", BLACK, 20);
  y_margin += 60;
  SimbleeForMobile.drawText(x_margin, y_margin, "Last Movement detected at 2245", BLACK, 13);  
   

  
  y_margin = SimbleeForMobile.screenHeight - bottomPaneHeight;

  int unit_width = SimbleeForMobile.screenWidth/3 - 5;
  
  SimbleeForMobile.drawRect(0,y_margin,unit_width, bottomPaneHeight,rect_color);
  add_profile_id = SimbleeForMobile.drawButton(0 , y_margin + 5,unit_width, "Add Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(add_profile_id, EVENT_RELEASE);

  SimbleeForMobile.drawRect(unit_width+5,y_margin,unit_width+7,bottomPaneHeight,rect_color);
  edit_profile_id = SimbleeForMobile.drawButton(unit_width+5,y_margin + 5,unit_width+7, "Edit Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(edit_profile_id, EVENT_RELEASE);  

  SimbleeForMobile.drawRect(SimbleeForMobile.screenWidth - unit_width,y_margin,unit_width,bottomPaneHeight,rect_color);  
  delete_profile_id = SimbleeForMobile.drawButton(SimbleeForMobile.screenWidth - unit_width,y_margin + 5,unit_width, "Delete Profile", BLUE, TEXT_TYPE); 
  SimbleeForMobile.setEvents(delete_profile_id, EVENT_RELEASE);  

  SimbleeForMobile.setEvents(add_profile_id, EVENT_RELEASE);
  SimbleeForMobile.setEvents(edit_profile_id, EVENT_RELEASE);
  SimbleeForMobile.setEvents(delete_profile_id, EVENT_RELEASE);
  
  SimbleeForMobile.endScreen();
  need_refresh = true;
  need_restore = true;
  setupdone = 1;  
}

void ui()
{
  
  setup_home_screen();
}











