#include <nrk.h>
#include <include.h>
#include <ulib.h>
#include <stdio.h>
#include <avr/sleep.h>
#include <hal.h>
#include <bmac.h>
#include <nrk_error.h>

#include <nrk_driver_list.h>
#include <nrk_driver.h>
#include <ff_basic_sensor.h>

#define NODE_ID			2
#define ROOM_ID			1
#define SENSOR_SAMPLES	10
#define BMAC_CHANNEL	19
#define BEACON_COUNT	4
#define BEACON_TIME_OUT 10



uint8_t tx_buf[24];
uint8_t rx_buf[50];
nrk_time_t curr_time;

nrk_task_type MULTI_SENSING_TASK;
NRK_STK sensors_task_stack[NRK_APP_STACKSIZE];
void read_sensors (void);

nrk_task_type TX_TASK;
NRK_STK tx_task_stack[NRK_APP_STACKSIZE];
void tx_task (void);

nrk_task_type RX_TASK;
NRK_STK rx_task_stack[NRK_APP_STACKSIZE];
void rx_task (void);

void nrk_create_taskset ();
void nrk_register_drivers();

struct beacon_packet
{
	uint8_t header[2];
	uint8_t ID;
	int encryption_key;
};

struct node_packet
{
	uint8_t header[2];
	uint8_t ID;
	uint8_t beacon_rssi[BEACON_COUNT*2];
	uint8_t beacon_last_seen[BEACON_COUNT*2];
	int temperature[2], light[2], pir[2], humidity[2];
};



struct node_packet packet;



void init_node()
{
	packet.header[0] = 'N';
	packet.header[1] = 'D';
	packet.ID = NODE_ID;
}

int main ()
{
	nrk_setup_ports ();
	nrk_setup_uart (UART_BAUDRATE_115K2);

	nrk_init ();

	nrk_led_clr (0);
	nrk_led_clr (1);
	nrk_led_clr (2);
	nrk_led_clr (3);

	nrk_time_set (0, 0);

	bmac_task_config ();
	nrk_register_drivers();

	// init bmac on channel 19
	bmac_init(BMAC_CHANNEL);
	init_node();
	bmac_rx_pkt_set_buffer(rx_buf,RF_MAX_PAYLOAD_SIZE);

	nrk_create_taskset ();
	nrk_start ();

  return 0;
}

void rx_task (void)
{
	uint8_t i,len, fd;
	int8_t rssi,val;
	uint8_t *local_rx_buf;
	uint8_t neighbour_id;
	struct node_packet *rcv_packet;
	struct beacon_packet *bpacket;
	printf( "rx_task PID=%d\r\n",nrk_get_pid());

	val=bmac_wait_until_rx_pkt();    
	//Out of waiting. Received our first packet!
	printf("The first packet is here\r\n");
	while(1)
    {
    	nrk_led_set (ORANGE_LED);
		local_rx_buf = bmac_rx_pkt_get(&len,&rssi);

		
		
		// Release the RX buffer ASAP so future packets can arrive 
		bmac_rx_pkt_release();		
		
		//printf("RX %c Task frm %d\r\n",local_rx_buf[0], local_rx_buf[2]-5);		
		
		//Check the packet received
		if(len == sizeof(packet))
		{
			rcv_packet = local_rx_buf;
			neighbour_id = 1 - NODE_ID;			
			//printf("Packet received from %d %d\r\n", neighbour_id, rssi);

			for(i = 0;i<(BEACON_COUNT*2);i++)
			{
				if(i<2)
				{
					packet.light[i] = rcv_packet->light[i];

					packet.humidity[i] = rcv_packet->humidity[i];

					packet.pir[i] = rcv_packet->pir[i];

					packet.temperature[i] = rcv_packet->temperature[i];
				}
				packet.beacon_rssi[i] = rcv_packet->beacon_rssi[i];
				packet.beacon_last_seen[i] = rcv_packet->beacon_last_seen[i];
			}

			//printf("l0 %d\t h0 %d\t p0 %d\t t0 %d\t",packet.light[0],packet.humidity[0],packet.pir[0],packet.temperature[0] );
			
			//printf("l1 %d\t h1 %d\t p1 %d\t t1 %d\t",packet.light[1],packet.humidity[1],packet.pir[1],packet.temperature[1] );
		}

		nrk_led_clr (ORANGE_LED);
		//Put the task in waiting mode till the next packet arrives
		val=bmac_wait_until_rx_pkt();  
    }	
}
void tx_task (void)
{
	uint8_t j, i, val, len;

	uint16_t nrk_max_sleep_wakeup_time;
	nrk_sig_t tx_done_signal;
	nrk_sig_mask_t ret;
	//nrk_time_t end_time;
	
	printf ("tx_task PID=%d\r\n", nrk_get_pid ());
	
	// Wait until the tx_task starts up bmac
	while (!bmac_started ())
		nrk_wait_until_next_period ();

	// Get and register the tx_done_signal if you want to
	// do non-blocking transmits
	tx_done_signal = bmac_get_tx_done_signal ();
	nrk_signal_register (tx_done_signal);


	while (1) 
	{

		//nrk_time_get(&end_time);
		//printf("TX ID Task %d\r\n", end_time.secs);

		// Build a TX packet
		nrk_led_set (RED_LED);
		
		// This function shows how to transmit packets in a
		// non-blocking manner  
		val = bmac_tx_pkt_nonblocking(&packet, sizeof(packet));
		// This functions waits on the tx_done_signal
		ret = nrk_event_wait (SIG(tx_done_signal));

		/*
		// Just check to be sure signal is okay
		if(ret & SIG(tx_done_signal) == 0 ) 
			nrk_kprintf (PSTR ("TX done signal error\r\n"));
		else tx_data_ok=1;
		*/
		
		// Task gets control again after TX complete
		//printf("Return to Tx ID after wait\r\n");
		nrk_led_clr (RED_LED);	

		nrk_wait_until_next_period ();
	}
}

void read_sensors()
{
	uint8_t fd;
	int16_t temp, t, h, p, l;
	uint8_t i, val;
	
	
	printf( "sensor_task PID=%d\r\n",nrk_get_pid());

	// Open ADC device as read 
	fd=nrk_open(FIREFLY_3_SENSOR_BASIC,READ);
	if(fd==NRK_ERROR) nrk_kprintf(PSTR("Failed to open sensor driver\r\n"));	

	while(1)
	{

		t = 0;
		h = 0;
		p = 0;
		l = 0;
		//printf("sensor\r\n");
		nrk_led_set(GREEN_LED); 
		
		//nrk_time_get(&tstime);


		
		


		printf("GW;%d;%d;%d;%d;",packet.beacon_rssi[0],packet.beacon_rssi[1],packet.beacon_rssi[2],packet.beacon_rssi[3]);
		printf("%d;%d;%d;%d;",packet.beacon_rssi[4],packet.beacon_rssi[5],packet.beacon_rssi[6],packet.beacon_rssi[7]);

			printf("%d;%d;%d;%d;",packet.light[0],packet.humidity[0],packet.pir[0],packet.temperature[0] );
			
			printf("%d;%d;%d;%d;\r\n",packet.light[1],packet.humidity[1],packet.pir[1],packet.temperature[1] );

		nrk_led_clr(GREEN_LED);	

	//	printf("%d\r\n",light[NODE_ID]&0xFF);
	//	printf("%d %d ",humidity[NODE_ID],light[NODE_ID] );

	//	printf("%d %d\r\n",pir[NODE_ID],temperature[NODE_ID] );		
		nrk_wait_until_next_period();

	}	
}

void nrk_create_taskset ()
{
  
  MULTI_SENSING_TASK.task = read_sensors;
  nrk_task_set_stk( &MULTI_SENSING_TASK, sensors_task_stack, NRK_APP_STACKSIZE);
  MULTI_SENSING_TASK.prio = 3;
  MULTI_SENSING_TASK.FirstActivation = TRUE;
  MULTI_SENSING_TASK.Type = BASIC_TASK;
  MULTI_SENSING_TASK.SchType = PREEMPTIVE;
  MULTI_SENSING_TASK.period.secs = 2;
  MULTI_SENSING_TASK.period.nano_secs = 0;
  MULTI_SENSING_TASK.cpu_reserve.secs = 2;
  MULTI_SENSING_TASK.cpu_reserve.nano_secs = 0;
  MULTI_SENSING_TASK.offset.secs = 0;
  MULTI_SENSING_TASK.offset.nano_secs = 0;
  nrk_activate_task (&MULTI_SENSING_TASK);   


  TX_TASK.task = tx_task;
  nrk_task_set_stk( &TX_TASK, tx_task_stack, NRK_APP_STACKSIZE);
  TX_TASK.prio = 2;
  TX_TASK.FirstActivation = TRUE;
  TX_TASK.Type = BASIC_TASK;
  TX_TASK.SchType = PREEMPTIVE;
  TX_TASK.period.secs = 3;
  TX_TASK.period.nano_secs = 0;
  TX_TASK.cpu_reserve.secs = 6;
  TX_TASK.cpu_reserve.nano_secs = 0;
  TX_TASK.offset.secs = 0;
  TX_TASK.offset.nano_secs = 0;
  nrk_activate_task (&TX_TASK);     
 
  RX_TASK.task = rx_task;
  nrk_task_set_stk( &RX_TASK, rx_task_stack, NRK_APP_STACKSIZE);
  RX_TASK.prio = 1;
  RX_TASK.FirstActivation = TRUE;
  RX_TASK.Type = BASIC_TASK;
  RX_TASK.SchType = PREEMPTIVE;
  RX_TASK.period.secs = 4;
  RX_TASK.period.nano_secs = 0;
  RX_TASK.cpu_reserve.secs = 6;
  RX_TASK.cpu_reserve.nano_secs = 0;
  RX_TASK.offset.secs = 0;
  RX_TASK.offset.nano_secs = 0;
  nrk_activate_task (&RX_TASK);  

}


void nrk_register_drivers()
{
	int8_t val;

	val=nrk_register_driver( &dev_manager_ff3_sensors,FIREFLY_3_SENSOR_BASIC);
	if(val==NRK_ERROR) nrk_kprintf( PSTR("Failed to load my ADC driver\r\n") );

}
