/*
TO DO LIST:

1. SIGKILL Handler (Ctrl+C): For Emergency Brake (Lower the skids, then hit both brakes)
2. MODE Selectors: Manual and Auto
3. Functions to parse distance, velocity and Pot Values from get_data
4. TIMEOUT for non-responsive read commands
5. Auto-control algorithm: NEEDS a lot of #define parameters like LEV_VELOCITY, LEV_DISTANCE, etc
6. Time based auto-mode for the vacuum chamber
7. Validation functions for feedback for each command  (for eg: getMCU1Data should have a cetain no of ';'s, etc)
8. Automatic linkage of MCU device (ACMx) with the file descriptors(fd_mcux) and rio_mcux based on their ID.
9. This program will run in background for the auto-pilot mode. Another logging program will run in the foreground. That should be able
	to send this one a sigkill (for emergency braking).
10.All commands sent should be acknowledged by the MCU and verified. If not, try again for a fixed no of times and then declare an error
11.This program will take in command-line arguments on whether it should be manual or automatic. This is for the webserver program to 
	run this code using execve.
12.Test and interface the current sensor
13.Install Screen and see if that can be used to interact with USB devices

*/

#define LEV_LEFT_RET_bm			0x01
#define LEV_LEFT_EXT_bm			0x02
		
#define LEV_RIGHT_RET_bm		0x04
#define LEV_RIGHT_EXT_bm		0x08
		
#define MECH_BRAKE_RET_bm		0x10
#define MECH_BRAKE_EXT_bm		0x20
		
#define SERV_PROP_FOR_bm		0x40
#define SERV_PROP_REV_bm		0x80
		
#define MAG_BRAKE_RET_bm		0x100
#define MAG_BRAKE_EXT_bm		0x200

#define MAG_BRAKE_1_RET_bm		0x01
#define MAG_BRAKE_1_EXT_bm		0x02
	
#define MAG_BRAKE_2_RET_bm		0x04
#define MAG_BRAKE_2_EXT_bm		0x08
		
#define MAG_BRAKE_3_4_RET_bm	0x10
#define MAG_BRAKE_3_4_EXT_bm	0x20



#define RECEIVE_BUFFER_LENGTH 	100
#define WRITE_BUFFER_LENGTH 	100

/*Commands for MCU1*/
#define getID				"ID"
#define get_LEV_LEFT_POT	"1C2"
#define get_LEV_RIGHT_POT	"1C3"
#define getADC2     		"1C4"
#define getMCU1ADC      	"1C5"
#define getSTATE_1    		"1C6"
#define setSTATE_1    		"1C7"
#define getMCU1Data     	"1C8"
#define getDistance 		"1C9"
#define checkDSensor		"1C10"
#define checkMCU1POTS   	"1C11"

/*Commands for MCU2*/

#define getID				"ID"
#define getMagBrake1Pot 	"2C1"
#define getMagBrake2Pot 	"2C2"
#define getMagBrake34Pot	"2C3"
#define getMagBrakePots 	"2C4"
#define getSTATE_2    		"2C5"
#define setSTATE_2    		"2C6"
#define checkMCU2POTS   	"2C7"
#define getMCU2Data			"2C8"

/*Commands for MCU3*/

#define getID				"ID"
#define getHS_FRONT_LEFT	"3C1"
#define getHS_FRONT_RIGHT	"3C2"
#define getHS_REAR_LEFT		"3C3"
#define getHS_Data     		"3C4"
#define getMCU3Data     	"3C5"
#define checkMCU3POTS   	"3C8"



#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include<time.h>
#include"csapp.h"

void get_data(rio_t *reader, int fd_mcu, char *command, char *response);
int get_distance();
int init_file();
void exit_handler(int signal);
void get_time(char* time_buf);

static int DATA_INDEX = 0;

static time_t START_TIME = 0, ELAPSED_TIME = 0;

static int TRACK_LENGTH = 0;

static int DISTANCE, VELOCITY, TEMPERATURE;
static uint16_t LEV_LEFT_POT, LEV_RIGHT_POT;
static int STATE_1, PUSHER_STATE;

static int ADC_READABLE_1 = 0;

static uint16_t MAG_BRAKE_1_POT, MAG_BRAKE_2_POT, MAG_BRAKE_3_4_POT;

static int LEV_L_POT_OK, LEV_R_POT_OK, HS_RE_RT_OK, D_SENSOR_OK;

static int MAG_1_POT_OK, MAG_2_POT_OK, MAG_3_4_POT_OK;

static int HS_FR_LT_OK, HS_FR_RT_OK, HS_RE_LT_OK;

static int STATE_2;

static int ADC_READABLE_2 = 0;

static uint16_t HS_FR_LT, HS_FR_RT, HS_RE_LT, HS_RE_RT;	// HS_RE_RT from MCU1

static int exit_status, manual_input;

static char receive_buffer[RECEIVE_BUFFER_LENGTH];

static char write_buffer[WRITE_BUFFER_LENGTH], read_buffer[50];

static int fd_mcu1,fd_mcu2,fd_mcu3 ;

static rio_t rio_mcu1,rio_mcu2,rio_mcu3;

FILE *fd_log;


void exit_handler(int signal)
{
	exit_status = 1;
}

void manual_input_handler(int signal)
{
	manual_input = 1;
}

void init()
{
	init_file();
	DISTANCE 	   = 0;
	VELOCITY       = 0;
	TEMPERATURE    = 0;
	LEV_LEFT_POT   = 0;
	LEV_RIGHT_POT  = 0;
}

int init_file(char *dev_name)
{
	rio_t rio_tmp;
	int fd_tmp = open(dev_name, O_RDWR);
	if (fd_tmp < 0){
	  perror("Failed to open the AVR device");
	  return errno;
	}
	
	Rio_readinitb(&rio_tmp, fd_tmp);
	strcpy(receive_buffer,getID);
	memset(read_buffer,0,50);
	get_data(&rio_tmp,fd_tmp,receive_buffer ,read_buffer);

	if(strcmp(read_buffer,"mcu1\n")==0)
	{
		fd_mcu1 = fd_tmp;
		Rio_readinitb(&rio_mcu1, fd_mcu1);
		printf("fd_mcu1: Device = %s Read = %s\n",dev_name, read_buffer );
	}
	if(strcmp(read_buffer,"mcu2\n")==0)
	{
		fd_mcu2 = fd_tmp;
		Rio_readinitb(&rio_mcu2, fd_mcu2);
		printf("fd_mcu2: Device = %s Read = %s\n",dev_name, read_buffer );
	}	
	if(strcmp(read_buffer,"mcu3\n")==0)
	{
		fd_mcu3 = fd_tmp;
		Rio_readinitb(&rio_mcu3, fd_mcu3);
		printf("fd_mcu3: Device = %s Read = %s\n",dev_name, read_buffer );
	}			
	return 0;
}

void get_time(char* time_buf)
{ 
  /* From @Chris (stackoverflow) */
  char buffer[26];
  int millisec;
  struct tm* tm_info;
  struct timeval tv;
  gettimeofday(&tv, NULL);
  millisec = (int)(tv.tv_usec/1000.0); /* Round to nearest millisec */
  if (millisec >= 1000) /* Allow for rounding up to nearest second */
  { 
    millisec -=1000;
    tv.tv_sec++;
  }
  tm_info = localtime(&tv.tv_sec);
  strftime(buffer, 26, "%D %H:%M:%S", tm_info);
  sprintf(time_buf,"%s.%03d", buffer, millisec);
}

void write_to_avr(int fd_mcu, char *data, char *header, char *terminator)
{
	strcpy(write_buffer, header);
	strcat(data,terminator);
	strcpy(write_buffer, data);
	Rio_writen(fd_mcu,write_buffer,strlen(write_buffer));
//	write(fd_mcu1,write_buffer,strlen(write_buffer));	
}

void get_data(rio_t *reader, int fd_mcu, char *command, char *response)
{
	write_to_avr(fd_mcu, command,"mcu1","\n");
	Rio_readlineb(reader, response, 50);	
}

int count_char(char *str, int len, char c)
{
	int i = 0, count = 0;
	for(i = 0; i<len;i++)
	{
		if(str[i] == c)
			count++;
	}	
	return count;
}


int check_d_sensor()
{
		char *ds;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, checkDSensor);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
		if(count_char(read_buffer, 50, ';') != 1)
		{
			printf("Error reading Distance Sensor Status\n");
			return -1;
		}

		ds = strtok(read_buffer,";");
		
		D_SENSOR_OK = atoi(ds);
		
		return 0;
}


int check_mcu_1_adc()
{
		char *adc1, *adc2, *adc3;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, checkMCU1POTS);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
		if(count_char(read_buffer, 50, ';') != 3)
		{
			
			printf("Error reading MCU1 ADC Status\n");
			return -1;
		}

		adc1 = strtok(read_buffer,";");
		adc2 = strtok(NULL,";");
		adc3 = strtok(NULL,";");
		
		LEV_L_POT_OK = atoi(adc1);
		LEV_R_POT_OK = atoi(adc2);
		HS_RE_RT_OK = atoi(adc3);
		return 0;
}


int check_mcu_2_adc()
{
		char *adc1, *adc2, *adc3;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, checkMCU2POTS);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
		if(count_char(read_buffer, 50, ';') != 3)
		{
			printf("Error reading MCU2 ADC Status\n");
			return -1;
		}

		adc1 = strtok(read_buffer,";");
		adc2 = strtok(NULL,";");
		adc3 = strtok(NULL,"3");
		
		MAG_1_POT_OK = atoi(adc1);
		MAG_2_POT_OK = atoi(adc2);
		MAG_3_4_POT_OK = atoi(adc3);
		return 0;
}

int check_mcu_3_adc()
{
		char *adc1, *adc2, *adc3;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, checkMCU3POTS);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu3,fd_mcu3,receive_buffer ,read_buffer);
		if(count_char(read_buffer, 50, ';') != 3)
		{
			printf("Error reading MCU3 ADC Status\n");
			return -1;
		}

		adc1 = strtok(read_buffer,";");
		adc2 = strtok(NULL,";");
		adc3 = strtok(NULL,";");
		
		HS_FR_LT_OK = atoi(adc1);
		HS_FR_RT_OK = atoi(adc2);
		HS_RE_LT_OK = atoi(adc3);
		return 0;
}

int getSTATE_mcu1()
{
	char *state, *adc_readable;
	memset(receive_buffer,0,100);
	strcpy(receive_buffer, getSTATE_1);
	
	memset(read_buffer,0,50);
	get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
	//printf("\n Attempt..%s\n", read_buffer);
	
	if(count_char(read_buffer, 30, ';') != 1)
		return -1;

	state = strtok(read_buffer,";");
	adc_readable = strtok(NULL, "\n");
	STATE_1 = atoi(state);
	ADC_READABLE_1 = atoi(adc_readable);
	return 0;	
}

int getSTATE_mcu2()
{
	char *state, *adc_readable;
	memset(receive_buffer,0,100);
	strcpy(receive_buffer, getSTATE_2);
	
	memset(read_buffer,0,50);
	get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
	//printf("\n STATE2..%s\n", read_buffer);
	
	if(count_char(read_buffer, 30, ';') != 1)
		return -1;

	state = strtok(read_buffer,";");
	adc_readable = strtok(NULL, "\n");
	STATE_2 = atoi(state);
	ADC_READABLE_2 = atoi(adc_readable);
	return 0;	
}

int get_distance()
{
		char *dist, *vel, *temp;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, getDistance);
		int d,v,t;
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
		//printf("\n Attempt..%s\n", read_buffer);

		if(count_char(read_buffer, 30, ';') != 2)
			return -1;

        dist = strtok(read_buffer,";");
        vel  = strtok(NULL,";");
        temp = strtok(NULL, "\n");
		d = atoi(dist);
		v = atoi(vel);
		t = atoi(temp);		
		if(d == -1)
			return -1;
		DISTANCE = d;
		VELOCITY = v;
		TEMPERATURE = t;
		return 0;
}



int get_mcu_1_data()
{
		char *state,*left_pot, *right_pot,*h_re_rt,*dist, *vel, *temp,*pusher;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, getMCU1Data);
		int d,v,t;
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
		//printf("\n Attempt..%s\n", read_buffer);

		if(count_char(read_buffer, 50, ';') != 7)
		{
			printf("Error from MCU 1\n");
			return -1;
		}

		state = strtok(read_buffer,";");
		left_pot = strtok(NULL,";");
		right_pot = strtok(NULL,";");
		h_re_rt = strtok(NULL,";");
        dist = strtok(NULL,";");
        vel  = strtok(NULL,";");
        temp = strtok(NULL,";");
		pusher = strtok(NULL, "\n");
		STATE_1 = atoi(state);
		
		LEV_LEFT_POT = atoi(left_pot);
		LEV_RIGHT_POT = atoi(right_pot);
		HS_RE_RT = atoi(h_re_rt);
		PUSHER_STATE = atoi(pusher);
		
		d = atoi(dist);
		v = atoi(vel);
		t = atoi(temp);		
		if(d == -1)
			return 0;
		DISTANCE = d;
		VELOCITY = v;
		TEMPERATURE = t;
		return 0;
}

int get_mcu_2_data()
{
		char *state,*mag1pot, *mag2pot,*mag3pot;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, getMCU2Data);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
		
		if(count_char(read_buffer, 50, ';') != 3)
		{
			printf("Error from MCU 2\n");
			return -1;
		}

		state = strtok(read_buffer,";");
		mag1pot = strtok(NULL,";");
		mag2pot = strtok(NULL,";");
		mag3pot = strtok(NULL,"\n");

		STATE_2 = atoi(state);
		
		MAG_BRAKE_1_POT = atoi(mag1pot);
		MAG_BRAKE_2_POT = atoi(mag2pot);
		MAG_BRAKE_3_4_POT = atoi(mag3pot);

		return 0;
}

int get_mcu_3_HS_data()
{
		char  *fr_lt, *fr_rt, *re_lt;
		memset(receive_buffer,0,100);
		strcpy(receive_buffer, getHS_Data);
		
		memset(read_buffer,0,50);
		get_data(&rio_mcu3,fd_mcu3,receive_buffer ,read_buffer);
		//printf("\n Attempt..%s\n", read_buffer);

		if(count_char(read_buffer, 50, ';') != 2)
		{
			printf("Error from MCU 3\n");
			return -1;
		}
		

		fr_lt = strtok(read_buffer,";");
		fr_rt = strtok(NULL,";");
		re_lt = strtok(NULL,";");

		HS_FR_LT = atoi(fr_lt);		
		HS_FR_RT = atoi(fr_rt);
		HS_RE_LT = atoi(re_lt);

		return 0;
}



void setSTATE_mcu1(int STATE)
{
	memset(receive_buffer,0,100);
	strcpy(receive_buffer, setSTATE_1);
    sprintf(receive_buffer,"%s-%d",receive_buffer, STATE);
	
	memset(read_buffer,0,50);
	get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
	if(count_char(read_buffer, 50, ';') != 1)
	{
		printf("Error from MCU 1 manual\n");
		setSTATE_mcu1(STATE);
	}		
	
	printf("Received from AVR1: %s\n",read_buffer);
	return;	
}

void setSTATE_mcu2(int STATE)
{
	memset(receive_buffer,0,100);
	strcpy(receive_buffer, setSTATE_2);
    sprintf(receive_buffer,"%s-%d",receive_buffer, STATE);
	
	memset(read_buffer,0,50);
	get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
	
	if(count_char(read_buffer, 50, ';') != 1)
	{
		printf("Error from MCU 2 manual\n");
		setSTATE_mcu2(STATE);
	}	
	printf("Received from AVR2: %s\n",read_buffer);
	return;	
}


void print_diag_status()
{
	printf("\n\t\tInitial Diagnostics Status Report:\n");
	
	if(D_SENSOR_OK)
	{
		printf("\nDistance Sensor: OK\t\tTRACK LENGTH = %d",TRACK_LENGTH);
	}	
	else
		printf("\nDistance Sensor: NOT CONNECTED");
	
	printf("\nLEVITATION ACTUATOR POTS: \tLEFT: %d \tRIGHT: %d",LEV_L_POT_OK,LEV_R_POT_OK);
	printf("\nMAG BRAKE ACTUATOR POTS: \tACT1: %d \tACT2 : %d \tACT3_4: %d\n",MAG_1_POT_OK,MAG_2_POT_OK, MAG_3_4_POT_OK);
	printf("\nHEIGHT SENSORS: \nFRONT LEFT: %d \tFRONT RIGHT: %d",HS_FR_LT_OK,HS_FR_RT_OK);
	printf("\nREAR  LEFT: %d \tREAR  RIGHT: %d",HS_RE_LT_OK,HS_RE_RT_OK);
}

void init_sequence()
{
	int choice, count = 5;
	
	check_mcu_1_adc();
	check_mcu_2_adc();
	check_mcu_3_adc();
	
	while(count > 0)
	{
		count--;
		check_d_sensor();		
		if(D_SENSOR_OK)
			break;
	}
	
	if(D_SENSOR_OK)
	{
		while(!DISTANCE)
			get_distance();
		TRACK_LENGTH = DISTANCE;	
		
	}

	print_diag_status();

	if(D_SENSOR_OK && LEV_L_POT_OK && LEV_R_POT_OK && MAG_1_POT_OK && MAG_2_POT_OK
		&& MAG_3_4_POT_OK && HS_FR_LT_OK && HS_FR_RT_OK && HS_FR_RT_OK && 
		HS_RE_LT_OK && HS_RE_RT_OK)
		{
			printf("\nAll Sensors healthy..\n\n");
		}
		else
		{
			printf("\n\n\nSome Sensors are faulty or improperly connected. Press 1 to abort, or any other key to continue..\n\n");
			scanf("%d",&choice);
			if(choice == 1)
			{
				exit(1);
			}			
		}
}


void manual_input_sequence()
{
	int choice = 0;
	manual_input = 0;
	//clk = clock();
	//memset(receive_buffer,0,100);
	//memset(read_buffer,0,50);
	//printf("\nSend to AVR1:");
	//scanf("%s",receive_buffer);
	//get_data(&rio_mcu1,fd_mcu1,receive_buffer ,read_buffer);
	printf("\n\n");
	printf("1. Retract Levitation Skids\n");
	printf("2. Extend Levitation Skids\n\n");
	
	printf("3. Engage Magnetic Brakes\n");
	printf("4. Disengage Magnetic Brakes\n\n");	
	
	printf("5. Engage Mech Brakes\n");
	printf("6. Disengage Mech Brakes\n\n");	

	printf("7. Service Propulsion ON\n");
	printf("8. Service Propulsion OFF\n\n");	

	scanf("%d",&choice);

	switch(choice)
	{
		case 1:	setSTATE_mcu1(STATE_1 | LEV_LEFT_RET_bm | LEV_RIGHT_RET_bm);
				ADC_READABLE_1 = 0;
				break;
		case 2:	setSTATE_mcu1(STATE_1 | LEV_LEFT_EXT_bm | LEV_RIGHT_EXT_bm);
				ADC_READABLE_1 = 0;
				break;		
		case 3: setSTATE_mcu2(STATE_2 | MAG_BRAKE_1_RET_bm | MAG_BRAKE_2_RET_bm | MAG_BRAKE_3_4_EXT_bm);
				ADC_READABLE_2 = 0;
				break;
		case 4: setSTATE_mcu2(STATE_2 | MAG_BRAKE_1_EXT_bm | MAG_BRAKE_2_EXT_bm | MAG_BRAKE_3_4_RET_bm);
				ADC_READABLE_2 = 0;				
				break;
		case 5:	setSTATE_mcu1(STATE_1 | MECH_BRAKE_EXT_bm);
				break;	
		case 6:	setSTATE_mcu1(STATE_1 | MECH_BRAKE_RET_bm);
				break;		
		default: break;
	}

	/*
	char time_stamp[30];
	get_time(time_stamp);
	printf("%s: ",time_stamp);
	printf("Read bytes from AVR1: %s\n\r",read_buffer);		*/			
}

void data_monitor()
{
	//clk = clock();
//	memset(receive_buffer,0,100);
//	memset(read_buffer,0,50);
//	strcpy(receive_buffer,"2C4");
//	printf("\nSend to AVR:");
//	scanf("%s",receive_buffer);
//	get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
//	get_data(&rio_mcu2,fd_mcu2,receive_buffer ,read_buffer);
	//get_distance();
	//fprintf(fd_log,"%ld: ",clk);
	
	char time_stamp[30];
	get_time(time_stamp);
	ELAPSED_TIME = time(NULL) - START_TIME;
	char pusher_state[15], lev_status[15], mag_brake_status[15], mech_brake_status[15], serv_status[15];

	if(ADC_READABLE_2)
	{
		get_mcu_2_data();
	}	
	else
	{
		getSTATE_mcu2();	
	}
	
	
	if(ADC_READABLE_1)
	{
		get_mcu_1_data();
	}
	else
	{
		getSTATE_mcu1();
		get_distance();		
	}
	get_mcu_3_HS_data();
	
	if(STATE_1 & MECH_BRAKE_RET_bm) 
		strcpy(mech_brake_status, "RETRACTING");
	else if(STATE_1 & MECH_BRAKE_EXT_bm)
		strcpy(mech_brake_status, "EXTENDING");
	else
		strcpy(mech_brake_status, "OFF");

	if(STATE_1 & SERV_PROP_FOR_bm) 
		strcpy(serv_status, "FORWARD");
	else if(STATE_1 & SERV_PROP_REV_bm)
		strcpy(serv_status, "REVERSE");
	else
		strcpy(serv_status, "OFF");	
	
	if((STATE_1 & LEV_LEFT_RET_bm) || (STATE_1 & LEV_RIGHT_RET_bm))
		strcpy(lev_status, "RETRACTING");
	else if((STATE_1 & LEV_LEFT_EXT_bm) || (STATE_1 & LEV_RIGHT_EXT_bm))
		strcpy(lev_status, "EXTENDING");
	else
		strcpy(lev_status, "OFF");		
	
	if((STATE_2 & MAG_BRAKE_1_RET_bm) || (STATE_2 & MAG_BRAKE_2_RET_bm))
		strcpy(mag_brake_status, "RETRACTING");
	else if((STATE_2 & MAG_BRAKE_1_EXT_bm) || (STATE_2 & MAG_BRAKE_2_EXT_bm))
		strcpy(mag_brake_status, "EXTENDING");
	else
		strcpy(mag_brake_status, "OFF");	
	
	if(PUSHER_STATE)
		strcpy(pusher_state, "ATTACHED");
	else
		strcpy(pusher_state, "DETACHED");
	
	
	
	fprintf(fd_log,"%d.  %s: ",DATA_INDEX++,time_stamp);
	fprintf(fd_log, " Distance = %d\tVelocity = %d\tTemperature = %d\n",DISTANCE, VELOCITY, TEMPERATURE );
	
	fprintf(fd_log, "\n\t\t\tELAPSED_TIME = %d s",ELAPSED_TIME);
	fprintf(fd_log, "\nLEV. SKIDS: %s \tMAGN BRAKES: %s \tMECH BRAKES: %s \tSERV. PROP.: %s\n", lev_status,mag_brake_status, mech_brake_status, serv_status);
	
	fprintf(fd_log,"\nMCU1 ADC READABLE: %d \tMCU2 ADC READABLE: %d \tPUSHER: %s\n", ADC_READABLE_1,ADC_READABLE_2, pusher_state);
	
	fprintf(fd_log, "\nLEV_L_POT = %d \tLEV_R_POT = %d", LEV_LEFT_POT, LEV_RIGHT_POT);
	
	fprintf(fd_log, "\nMAG_1_POT = %d \tMAG_2_POT = %d \tMAG_3_4_POT = %d\n", MAG_BRAKE_1_POT, MAG_BRAKE_2_POT,MAG_BRAKE_3_4_POT);	
	
	fprintf(fd_log,"\nHEIGHT SENSORS: \nFRONT LEFT = %d \tFRONT RIGHT = %d \nREAR  LEFT = %d \tREAR  RIGHT = %d ", HS_FR_LT, HS_FR_RT, HS_RE_LT, HS_RE_RT);
	
	fprintf(fd_log,"\n\n\n\r");
	
	fflush(fd_log);			
}

static void wait_monitor(int wait)
{
	int init_TIME = time(NULL);
	while((time(NULL) - init_TIME) != wait)
		data_monitor();	
}

void time_based_mode()
{
	int TIME, init_TIME;
	while(exit_status == 0)
	{
		init_TIME = time(NULL);
		
		// Wait for 5 seconds
		wait_monitor(5);
		
		/*Retract Levitation Skids*/
		setSTATE_mcu1(STATE_1 | LEV_LEFT_RET_bm | LEV_RIGHT_RET_bm);
		ADC_READABLE_1 = 0;		
		
		// Wait for 10 seconds
		wait_monitor(10);
		
		/*Engage Mech Brakes*/
		setSTATE_mcu1(STATE_1 | MECH_BRAKE_EXT_bm);	
		
		// Wait for 10 seconds
		wait_monitor(10);	
		
		/*Disengage Mech Brakes*/
		setSTATE_mcu1(STATE_1 | MECH_BRAKE_RET_bm);
		
		// Wait for 10 seconds
		wait_monitor(10);			

		/*Extend Levitation Skids*/
		setSTATE_mcu1(STATE_1 | LEV_LEFT_EXT_bm | LEV_RIGHT_EXT_bm);
		ADC_READABLE_1 = 0;		
		
		// Wait for 5 seconds
		wait_monitor(5);
		
		/*End of time period*/
			
	}		
}

void manual_mode()
{
	while(exit_status == 0)
	{
		if(manual_input)
			manual_input_sequence();
		else
			data_monitor();
	}	
}

int main()
{
	int choice = 0;
	init_file("/dev/ttyACM0");
	init_file("/dev/ttyACM1");
	init_file("/dev/ttyACM2");
	int len = 0;
	int i = 0;
	exit_status = 0;
	manual_input = 0;
	clock_t clk;
	fd_log  = fopen("/home/pi/test_uart/log.txt", "w");
	
	if(signal(SIGTSTP, exit_handler) == SIG_ERR)
	{
		printf("Couldn't initialize the signal handler. The program will not terminate in a stable manner\n");
	}
	if(signal(SIGINT, manual_input_handler) == SIG_ERR)
	{
		printf("Couldn't initialize the signal handler. The program will not switch to manual mode in a stable manner\n");
	}	
	
	init_sequence();
	
	
	while((choice<1)||(choice>3))
	{
		printf("1. Manual mode\n2.Time-based Run (Vacuum Chamber Test)\n3.Distance-based Run");		
		scanf("%d",&choice);
	}
	START_TIME = time(NULL);
	if(choice == 1)
		manual_mode();
	else if(choice == 2)
		time_based_mode();
	
	fclose(fd_log);
	return 0;
}

