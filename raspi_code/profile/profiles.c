#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include<time.h>
#include"csapp.h"


#define ARDUINO_DEVICE_NAME		"/dev/ttyACM0"
#define FIREFLY_DEVICE_NAME		"/dev/ttyUSB0"
#define SIMBLEE_DEVICE_NAME		"/dev/ttyUSB1"

#define PRESETS_FILE 			"../presets.txt"
#define PRESENT_VAL_FILE 		"../present_val.txt"
#define FIREFLY_DATA_FILE 		"../firefly_values.txt"

#define RECEIVE_BUFFER_LENGTH 	100
#define WRITE_BUFFER_LENGTH 	100
#define BEACON_COUNT			4
#define MAX_PRESETS				3

static int fd_gateway, fd_arduino, fd_simblee;
size_t stored_presets_count;

FILE *fd_log,*fd_presets, *fd_present_data;

uint8_t rcv_data[100];
int request_profiles;

static char receive_buffer[RECEIVE_BUFFER_LENGTH], buffer[500];
static char write_buffer[WRITE_BUFFER_LENGTH], simblee_buffer[100], arduino_buffer[100];

struct node_packet
{
	uint8_t header[2];
	uint8_t ID;
	uint8_t beacon_rssi[BEACON_COUNT*2];
	uint8_t beacon_last_seen[BEACON_COUNT*2];
	int temperature[2], light[2], pir[2], humidity[2];
};

struct arduino_packet
{
	int id;
	int r,g,b;
	int fan, humidifier;
};

int temp, hum, pir, light_val, rssi_1,rssi_2,rssi_3;

int exit_condition = 0, manual_input = 0;

struct node_packet packet;

struct arduino_packet presets[4], present_packet;

struct arduino_packet test_packet;
static rio_t rio_simblee, rio_arduino;

void write_to_arduino(struct arduino_packet *a_packet);

void init_simblee()
{
	fd_simblee = open(SIMBLEE_DEVICE_NAME, O_RDWR);
	Rio_readinitb(&rio_simblee, fd_simblee);	
}

void init_arduino()
{
	fd_arduino = open(ARDUINO_DEVICE_NAME, O_RDWR);
	Rio_readinitb(&rio_arduino, fd_arduino);	
}


void print_arduino_data(struct arduino_packet *a_pack)
{
	memset(simblee_buffer,0,100);
	sprintf(simblee_buffer,"AU:%d:%d:%d:%d:%d\n", 
		a_pack->r,a_pack->g,a_pack->b,
		a_pack->fan,a_pack->humidifier);
	printf("%s\n",simblee_buffer);
}

void write_presets_to_file(struct arduino_packet *b_packet)
{
	print_arduino_data(b_packet);
	fwrite(b_packet, sizeof(struct arduino_packet), 1, fd_presets);
}

void write_test_presets()
{
	fd_presets = fopen(PRESETS_FILE,"w");
	fill_dummy_data(0,&test_packet,200,250,0 );
	write_presets_to_file(&test_packet);		

	fill_dummy_data(1,&test_packet,0,200,136 );
	write_presets_to_file(&test_packet);		

	fill_dummy_data(2,&test_packet,200,0,150 );
	write_presets_to_file(&test_packet);
	//write_presets_to_file(&test_packet);	
	fclose(fd_presets);	
}

void write_to_arduino(struct arduino_packet *a_packet)
{
	char arduino_response[10];
	memset(arduino_buffer,0,100);
	sprintf(arduino_buffer,"AU:%d:%d:%d:%d:%dk", 
		a_packet->r,a_packet->g,a_packet->b,
		a_packet->fan,a_packet->humidifier);
	printf("%s",arduino_buffer);
	while(1)
	{
		Rio_writen(fd_arduino,arduino_buffer,strlen(arduino_buffer));
		Rio_readlineb(&rio_arduino, arduino_response, 10);
		printf("%s",arduino_response);
		if(arduino_response[0] == 'A' && arduino_response[2] == 'K')
			break;
	}	

}

void write_presets_to_simblee()
{
	printf("Write to Simblee!\n");
	int i = 0;
	char simblee_response[10];
	memset(simblee_buffer,0,100);
	sprintf(simblee_buffer,"SB;1;");
//SB;1;ID1;HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;
//SB;1;0;0;1;126;196;235;1;0;0;197;55;148;2;0;0;0;0;0;
	for(;i<3;i++)
	{
		sprintf(simblee_buffer,"%s%d;%d;%d;%d;%d;%d;",simblee_buffer,
			presets[i].id,presets[i].humidifier,presets[i].fan,
			presets[i].r,presets[i].g,presets[i].b);
	}

	printf("%s",simblee_buffer);
	Rio_readlineb(&rio_simblee, simblee_response, 10);
	printf("%s",simblee_response);	
	while(1)
	{
		Rio_writen(fd_simblee,simblee_buffer,strlen(simblee_buffer));
		Rio_readlineb(&rio_simblee, simblee_response, 10);
		printf("%s",simblee_response);
		if(simblee_response[0] == 'A' && simblee_response[2] == 'K')
			break;
	}	
	printf("Success!");

}

void session()
{
	char simblee_response[100];
	memset(simblee_buffer,0,100);
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);	
	if(request_profiles)
	{
		sprintf(simblee_buffer,"PI;1;");
		request_profiles = 0;
	}
	else
		sprintf(simblee_buffer,"PI;0;");

	sprintf(simblee_buffer,"%s%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;",simblee_buffer,temp,hum,light_val,pir,rssi_1,rssi_2,rssi_3,tm.tm_hour, tm.tm_min,tm.tm_sec);

	//PI;0/1/2;Temp;Hum;Light;PIR;b1;b2;b3;

//PI;0;35;49;95;100;78;78;78;

	printf("\nSending %s",simblee_buffer);

	Rio_writen(fd_simblee,simblee_buffer,strlen(simblee_buffer));
	Rio_readlineb(&rio_simblee, simblee_response, 100);


	parse_simblee(simblee_response, &present_packet);
	write_to_arduino(&present_packet);


}

void write_data_to_file(struct arduino_packet *pack)
{
	fd_present_data = fopen(PRESENT_VAL_FILE,"w");
	write_presets_to_file(pack);		
	fclose(fd_present_data);		
}

void read_presets_from_file()
{
	FILE *fd_presets = fopen(PRESETS_FILE,"r");
	size_t elem = fread(presets, sizeof(struct arduino_packet), 3, fd_presets);
	stored_presets_count = elem;
	printf("elements = %d\n",elem);
	
	
	int i = 0;
	for(;i<elem;i++)
	{
		print_arduino_data(presets + i);
	}	
	fclose(fd_presets);

	fill_dummy_data(3,presets+3,0,200,200 );
	(*(presets+3)).fan = 0;
	(*(presets+3)).humidifier = 0;	

}

void fill_dummy_data(int id,struct arduino_packet *b_packet, int x,int y, int z)
{
	int i;
	for(i = 0;i<4;i++)
	{
		b_packet->r = x;
		b_packet->g = y;
		b_packet->b = z;
	}
	b_packet->humidifier = 1;
	b_packet->fan = 1;
	b_packet->id = id;
}

void printPacket()
{
	int i;
	printf("\nBeacon ");
	for(i = 0;i<BEACON_COUNT*2;i++)
	{
		printf("%d ", packet.beacon_rssi[i]);
	}
	for(i = 0;i<2;i++)
	{
		printf("\nNode %d: Light %d humidity %d pir %d temperature %d \n",i,
			packet.light[i],packet.humidity[i],packet.pir[i],packet.temperature[i]);
	}
}


void read_firefly_data_from_file()
{
	FILE *fd_firefly_data = fopen(FIREFLY_DATA_FILE,"r");
	size_t elem = fread(&packet, sizeof(struct node_packet), 1, fd_firefly_data);
	fclose(fd_firefly_data);



	temp = 1024 - packet.temperature[0]<packet.temperature[1]?packet.temperature[0]:packet.temperature[1];

	temp = (temp * 100 / 1024) - 30;



	hum = (packet.humidity[0]>packet.humidity[1]?packet.humidity[0]:packet.humidity[1])*100/1024;

	light_val = (1024 - (packet.light[0]<packet.light[1]?packet.light[0]:packet.light[1]))*100 / 1024;


	pir = packet.pir[0]>packet.pir[1]?packet.pir[0]:packet.pir[1];

	rssi_1 = (packet.beacon_rssi[0] + packet.beacon_rssi[4])/2;
	rssi_2 = (packet.beacon_rssi[1] + packet.beacon_rssi[5])/2;
	rssi_3 = (packet.beacon_rssi[2] + packet.beacon_rssi[6])/2;

}


int count_char(char *str, int len, char c)
{
	int i = 0, count = 0;
	for(i = 0; i<len;i++)
	{
		if(str[i] == c)
			count++;
	}	
	return count;
}


int isValidData(char *data)
{
	if(strlen(data)<2)
		return -1;

	if(data[0] == 'G' && data[1] == 'W')
	{
		if(count_char(data, strlen(data), ';') != 17)
			return -1;
	}	
	else
		return -1;	

	return 0;
}

int isValidPresentValues(char *data)
{
	//SB;2;1;1;200;250;0;
	if(strlen(data)<2)
		return -1;

	if(data[0] == 'S' && data[1] == 'B')
	{
		if(count_char(data, strlen(data), ';') == 20)
		{

		}
		else if(count_char(data, strlen(data), ';') == 7)
		{

		}
		else
			return -1;
	}	
	else
		return -1;	

	return 0;
}


int parse_simblee(char *data, struct arduino_packet *pack)
{
	int i;

	if(isValidPresentValues(data)==-1)
		return -1;

	char *temp;


	if(count_char(data, strlen(data), ';') == 7)
	{
		//SB;2;1;1;200;250;0;
		temp = strtok(data,";");


		temp = strtok(NULL,";");
		request_profiles = atoi(temp);	

		temp = strtok(NULL,";");
		pack->fan = atoi(temp);		


		temp = strtok(NULL,";");
		pack->humidifier = atoi(temp);	

		temp = strtok(NULL,";");
		pack->r = atoi(temp);	

		temp = strtok(NULL,";");
		pack->g = atoi(temp);	

		temp = strtok(NULL,";");
		pack->b = atoi(temp);								

		printf("\nfan = %d hum = %d red = %d green = %d blue = %d",pack->fan,pack->humidifier,pack->r,pack->g,pack->b);		
	}

	if(count_char(data, strlen(data), ';') == 20)
	{

		//SB;1;ID1;HUM1;FAN1;R1;G1;B1;ID2;HUM2;FAN2;R2;G2;B2;ID3;HUM3;FAN3;R3;G3;B3;
		temp = strtok(data,";");


		temp = strtok(NULL,";");

		for(i = 0;i<3;i++)
		{
			temp = strtok(NULL,";");
			presets[i].fan = atoi(temp);					

			temp = strtok(NULL,";");
			presets[i].humidifier = atoi(temp);	

			temp = strtok(NULL,";");
			presets[i].r = atoi(temp);	

			temp = strtok(NULL,";");
			presets[i].g = atoi(temp);	

			temp = strtok(NULL,";");
			presets[i].b = atoi(temp);				
		}

		fd_presets = fopen(PRESETS_FILE,"w");
		for(i = 0;i<3;i++)
		{
			write_presets_to_file(presets+i);		
		}
		fclose(fd_presets);

	}	


	return 0;
}


void exit_handler()
{
	exit_condition = 1;
}

void display_profiles()
{
	printf("\n\n List of Existing Profiles:");
	int i;
	for(i = 0;i<stored_presets_count;i++)
	{
		
		printf("\n\n ID = %d", i);
		printf("\n LED: R = %d G = %d B = %d",
			presets[i].r,presets[i].g,presets[i].b);
		printf("\n Humidifier = %d\t Fan = %d\n\n", presets[0].humidifier,
			presets[0].fan);
	}
}



void install_sig_handlers()
{
	if(signal(SIGTSTP, exit_handler) == SIG_ERR)
	{
		printf("Couldn't initialize the signal handler. The program will not terminate in a stable manner\n");
	}

}


int main()
{
	request_profiles = 0;
	int i, min;
	init_arduino();
	init_simblee();

	//write_test_presets();
	
	install_sig_handlers();
	read_presets_from_file();
	write_presets_to_simblee();
	//write_to_arduino(presets+3);

	while(exit_condition == 0)
	{
		
		read_firefly_data_from_file();
		printPacket();
		session();
	}

/*
	while(exit_condition == 0)
	{
		if(manual_input == 1)
			menu();

		min = -1;
		printPacketToFile();
		for(i = 0;i<4;i++)
		{
			if((packet.beacon_rssi[i] + packet.beacon_rssi[i+4])/2)
			{
				min = i;
				break;
			}
		}


		if(min != -1)
		{
			write_to_arduino(presets+min);
		}
		else
			write_to_arduino(presets+3);
		
	}  */

	return 1;
}
