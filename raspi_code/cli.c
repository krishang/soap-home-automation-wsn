/*
 * C Program to Create User Record and Update it
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define MAX_USERS 3
int user_cnt = 1;

struct user
{
	int light_val_red;
	int light_val_blue;	
	int light_val_green;
	int humid_val;
	int temp_val;
	char name[50];
};

struct user user1, user2, user3;

void display();
void create();
void update();
 
int main()
{
    int ch;
 
    printf("1] Create a Profile\n");
    printf("2] Display Profiles\n");
    printf("3] Update Profile\n");
    printf("4] Exit");
    while (1)
    {
        printf("\n\nEnter your choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:    
            create();
            break;
        case 2:    
            display();
            break;
        case 3:    
            update();
            break;
        case 4: 
            exit(0);
        }
    }
}
 
/* To create an user record */
void create(){
	if(user_cnt <= MAX_USERS){
		if(user_cnt == 1){
			printf("Enter name of user : ");
			scanf("%s", user1.name);
			printf("Enter blue light intensity: ");
			scanf("%d", &(user1.light_val_blue));
			printf("Enter red light intensity: ");
			scanf("%d", &(user1.light_val_red));
			printf("Enter green light intensity: ");
			scanf("%d", &(user1.light_val_green));
			printf("Enter desired humidity: ");
			scanf("%d", &(user1.humid_val));
			printf("Enter desired temperature: ");
			scanf("%d", &(user1.temp_val));
			printf("%s - User 1 profile created\n", user1.name);
		}
		
		if(user_cnt == 2){
			printf("Enter name of user : ");
			scanf("%s", user2.name);
			printf("Enter blue light intensity: ");
			scanf("%d", &(user2.light_val_blue));
			printf("Enter red light intensity: ");
			scanf("%d", &(user2.light_val_red));
			printf("Enter green light intensity: ");
			scanf("%d", &(user2.light_val_green));
			printf("Enter desired humidity: ");
			scanf("%d", &(user2.humid_val));
			printf("Enter desired temperature: ");
			scanf("%d", &(user2.temp_val));
			printf("%s - User 2 profile created\n", user2.name);
		}
		
		if(user_cnt == 3){
			printf("Enter name of user : ");
			scanf("%s", user3.name);
			printf("Enter blue light intensity: ");
			scanf("%d", &(user3.light_val_blue));
			printf("Enter red light intensity: ");
			scanf("%d", &(user3.light_val_red));
			printf("Enter green light intensity: ");
			scanf("%d", &(user3.light_val_green));
			printf("Enter desired humidity: ");
			scanf("%d", &(user3.humid_val));
			printf("Enter desired temperature: ");
			scanf("%d", &(user3.temp_val));
			printf("%s - User 3 profile created\n", user3.name);
		}
		user_cnt++;
	}
	else
		printf("Maximum user count reached\n");
}
 
/* Display the records */
void display()
{   
	printf("\nUser 1:");
	printf("%s\n", user1.name);
	printf("Blue Light Intensity - %d\n", (user1.light_val_blue));
	printf("Red Light Intensity - %d\n", (user1.light_val_red));
	printf("Green Light Intensity - %d\n", (user1.light_val_green));
	printf("Humidity value - %d\n", (user1.humid_val));
	printf("Temperature value - %d\n\n", (user1.temp_val));
	
	printf("User 2:");
	printf("%s\n", user2.name);
	printf("Blue Light Intensity - %d\n", (user2.light_val_blue));
	printf("Red Light Intensity - %d\n", (user2.light_val_red));
	printf("Green Light Intensity - %d\n", (user2.light_val_green));
	printf("Humidity value - %d\n", (user2.humid_val));
	printf("Temperature value - %d\n\n", (user2.temp_val));
	
	printf("User 3:");
	printf("%s\n", user3.name);
	printf("Blue Light Intensity - %d\n", (user3.light_val_blue));
	printf("Red Light Intensity - %d\n", (user3.light_val_red));
	printf("Green Light Intensity - %d\n", (user3.light_val_green));
	printf("Humidity value - %d\n", (user3.humid_val));
	printf("Temperature value - %d\n\n", (user3.temp_val));
}
 
void update()
{
	int id, k;
	printf("Enter the id of the user to be updated\n");
	scanf("%d",&id);
	if(id > 3)
		printf("ID out of range\n");
	if(id == 1){
		printf("Enter the number according to the value to be updated\n");
		printf("1 : To edit the name of user\n");
		printf("2 : To edit the Red light intensity\n");
		printf("3 : To edit the Green light intensity\n");
		printf("4 : To edit the Blue light intensity\n");
		printf("5 : To edit the desired Humidity\n");
		printf("6 : To edit the desired Temperature\n");
		scanf("%d", &k);
		if(k ==1){
			printf("Enter new name\n");
			scanf("%s", user1.name);
			printf("Value updated\n");
		}
		else if(k == 2){
			printf("Enter new Red light intensity\n");				
			scanf("%d", &(user1.light_val_red));
			printf("Value updated\n");
		}
		else if(k == 3){
			printf("Enter new Green light intensity\n");				
			scanf("%d", &(user1.light_val_green));
			printf("Value updated\n");
		}
		else if(k == 4){
			printf("Enter new Blue light intensity\n");				
			scanf("%d", &(user1.light_val_blue));
			printf("Value updated\n");
		}		
		else if(k == 5){
			printf("Enter new Humidity\n");				
			scanf("%d", &(user1.humid_val));
			printf("Value updated\n");
		}
		else if(k == 6){
			printf("Enter new Temperature\n");				
			scanf("%d", &(user1.temp_val));
			printf("Value updated\n");
		}
		else
			printf("Incorrect number\n");
	}
	
	if(id == 2){
		printf("Enter the number according to the value to be updated\n");
		printf("1 : To edit the name of user\n");
		printf("2 : To edit the Red light intensity\n");
		printf("3 : To edit the Green light intensity\n");
		printf("4 : To edit the Blue light intensity\n");
		printf("5 : To edit the desired Humidity\n");
		printf("6 : To edit the desired Temperature\n");
		scanf("%d", &k);
		if(k ==1){
			printf("Enter new name\n");
			scanf("%s", user2.name);
			printf("Value updated\n");		
		}
		else if(k == 2){
			printf("Enter new Red light intensity\n");				
			scanf("%d", &(user2.light_val_red));
			printf("Value updated\n");
		}
		else if(k == 3){
			printf("Enter new Green light intensity\n");				
			scanf("%d", &(user2.light_val_green));
			printf("Value updated\n");
		}
		else if(k == 4){
			printf("Enter new Blue light intensity\n");				
			scanf("%d", &(user2.light_val_blue));
			printf("Value updated\n");
		}		
		else if(k == 5){
			printf("Enter new Humidity\n");				
			scanf("%d", &(user2.humid_val));
			printf("Value updated\n");
		}
		else if(k == 6){
			printf("Enter new Temperature\n");				
			scanf("%d", &(user2.temp_val));
			printf("Value updated\n");
		}
		else
			printf("Incorrect number\n");
	}
	
	if(id ==3){
		printf("Enter the number according to the value to be updated\n");
		printf("1 : To edit the name of user\n");
		printf("2 : To edit the Red light intensity\n");
		printf("3 : To edit the Green light intensity\n");
		printf("4 : To edit the Blue light intensity\n");
		printf("5 : To edit the desired Humidity\n");
		printf("6 : To edit the desired Temperature\n");
		scanf("%d", &k);
		if(k ==1){
			printf("Enter new name\n");
			scanf("%s", user3.name);
			printf("Value updated\n");
		}
		else if(k == 2){
			printf("Enter new Red light intensity\n");				
			scanf("%d", &(user3.light_val_red));
			printf("Value updated\n");
		}
		else if(k == 3){
			printf("Enter new Green light intensity\n");				
			scanf("%d", &(user3.light_val_green));
			printf("Value updated\n");
		}
		else if(k == 4){
			printf("Enter new Blue light intensity\n");				
			scanf("%d", &(user3.light_val_blue));
			printf("Value updated\n");
		}		
		else if(k == 5){
			printf("Enter new Humidity\n");				
			scanf("%d", &(user3.humid_val));
			printf("Value updated\n");
		}
		else if(k == 6){
			printf("Enter new Temperature\n");				
			scanf("%d", &(user3.temp_val));
			printf("Value updated\n");
		}
		else
			printf("Incorrect number\n");
	}
}