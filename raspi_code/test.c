#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include<time.h>
#include"csapp.h"

#define ARDUINO_DEVICE_NAME		"/dev/ttyACM0"
#define FIREFLY_DEVICE_NAME		"/dev/ttyUSB1"
#define SIMBLEE_DEVICE_NAME		"/dev/ttyUSB0"
#define FIREFLY_DATA_FILE 		"firefly_values.txt"

#define RECEIVE_BUFFER_LENGTH 	100
#define WRITE_BUFFER_LENGTH 	100
#define BEACON_COUNT			4
#define MAX_PRESETS				3

static int fd_gateway, fd_arduino;
size_t stored_presets_count;

FILE *fd_log;

uint8_t rcv_data[100];

static char receive_buffer[RECEIVE_BUFFER_LENGTH], buffer[500];
static char write_buffer[WRITE_BUFFER_LENGTH], arduino_buffer[100];

struct node_packet
{
	uint8_t header[2];
	uint8_t ID;
	uint8_t beacon_rssi[BEACON_COUNT*2];
	uint8_t beacon_last_seen[BEACON_COUNT*2];
	int temperature[2], light[2], pir[2], humidity[2];
};

struct arduino_packet
{
	int id;
	int r,g,b;
	int fan, humidifier;
};

int exit_condition = 0, manual_input = 0;

struct node_packet packet;

struct arduino_packet presets[4];

struct arduino_packet test_packet;
static rio_t rio_gateway, rio_arduino;


void init_arduino()
{
	fd_arduino = open(ARDUINO_DEVICE_NAME, O_RDWR);
	Rio_readinitb(&rio_arduino, fd_arduino);	
}

void init_firefly()
{
	fd_gateway = open(FIREFLY_DEVICE_NAME, O_RDWR);
	Rio_readinitb(&rio_gateway, fd_gateway);	
	fd_log  = fopen("log.txt", "w+");	
}

void write_to_arduino(struct arduino_packet *a_packet)
{
	char arduino_response[10];
	memset(arduino_buffer,0,100);
	sprintf(arduino_buffer,"AU:%d:%d:%d:%d:%dk", 
		a_packet->r,a_packet->g,a_packet->b,
		a_packet->fan,a_packet->humidifier);
	printf("%s",arduino_buffer);
	while(1)
	{
		Rio_writen(fd_arduino,arduino_buffer,strlen(arduino_buffer));
		Rio_readlineb(&rio_arduino, arduino_response, 10);
		printf("%s",arduino_response);
		if(arduino_response[0] == 'A' && arduino_response[2] == 'K')
			break;
	}	

}

void print_arduino_data(struct arduino_packet *a_pack)
{
	memset(arduino_buffer,0,100);
	sprintf(arduino_buffer,"AU:%d:%d:%d:%d:%d\n", 
		a_pack->r,a_pack->g,a_pack->b,
		a_pack->fan,a_pack->humidifier);
	printf("id=%d %s\n",a_pack->id, arduino_buffer);
}

void read_presets_from_file()
{
	FILE *fd_presets_read = fopen("presets.txt","r");
	size_t elem = fread(presets, sizeof(struct arduino_packet), 3, fd_presets_read);
	stored_presets_count = elem;
	printf("elements = %d\n",elem);
	
	
	int i = 0;
	for(;i<elem;i++)
	{
		print_arduino_data(presets + i);
	}	
	fclose(fd_presets_read);

	fill_dummy_data(4,presets+3,0,200,200 );
	(*(presets+3)).fan = 0;
	(*(presets+3)).humidifier = 0;	

}

void fill_dummy_data(int id, struct arduino_packet *b_packet, int x,int y, int z)
{
	int i;
	for(i = 0;i<4;i++)
	{
		b_packet->r = x;
		b_packet->g = y;
		b_packet->b = z;
	}
	b_packet->humidifier = 1;
	b_packet->fan = 1;
	b_packet->id = id;
}

void printPacket()
{
	int i;
	printf("\nBeacon ");
	for(i = 0;i<BEACON_COUNT*2;i++)
	{
		printf("%d ", packet.beacon_rssi[i]);
	}
	for(i = 0;i<2;i++)
	{
		printf("\nNode %d: Light %d humidity %d pir %d temperature %d \n",i,
			packet.light[i],packet.humidity[i],packet.pir[i],packet.temperature[i]);
	}
}

void printPacketToFile()
{
	readFireFlyGateway();
	//printf("%s \n",receive_buffer);
	char file_data[500];
	int i;
	sprintf(file_data,"\n");
	for(i = 0;i<2;i++)
	{
		fprintf(fd_log,"\n%sLight = %d\t Humidity = %d\t Temperature = %d\t PIR = %d\n",
			file_data,packet.light[i],packet.humidity[i],packet.temperature[i],packet.pir[i]);		
	}

	for(i = 0;i<8;i++)
	{
		fprintf(fd_log,"RSSI%d = %d\t",i,packet.beacon_rssi[i]);
	}

	fprintf(fd_log,"\n\n\r");
/*	if(receive_buffer[0] == 'L' && receive_buffer[1] == '3')
	{
		fprintf(fd_log,"\r");
		//printf("\n new record\n");
	}
	*/	

	fflush(fd_log);	
}

int count_char(char *str, int len, char c)
{
	int i = 0, count = 0;
	for(i = 0; i<len;i++)
	{
		if(str[i] == c)
			count++;
	}	
	return count;
}


int isValidData(char *data)
{
	if(strlen(data)<2)
		return -1;

	if(data[0] == 'G' && data[1] == 'W')
	{
		if(count_char(data, strlen(data), ';') != 17)
			return -1;
	}	
	else
		return -1;	

	return 0;
}


int parse_gateway(char *data, struct node_packet *pack)
{
	int i;

	if(isValidData(data)==-1)
		return -1;

	printf("%s",data);

	char *temp;

	temp = strtok(data,";");

	for(i = 0;i<BEACON_COUNT*2;i++)
	{
		temp = strtok(NULL,";");
		pack->beacon_rssi[i] = atoi(temp);		
	}

	for(i = 0;i<2;i++)
	{
		temp = strtok(NULL,";");
		pack->light[i] = atoi(temp);			

		temp = strtok(NULL,";");
		pack->humidity[i] = atoi(temp);	

		temp = strtok(NULL,";");
		pack->pir[i] = atoi(temp);	

		temp = strtok(NULL,";");
		pack->temperature[i] = atoi(temp);							
	}
	printPacket();
	return 0;
}

void readFireFlyGateway()
{
	Rio_readlineb(&rio_gateway, receive_buffer, 100);
	parse_gateway(receive_buffer, &packet);	

}

void exit_handler()
{
	exit_condition = 1;
}

void manual_input_handler(int signal)
{
	manual_input = 1;
}

void display_profiles()
{
	printf("\n\n List of Existing Profiles:");
	int i;
	for(i = 0;i<stored_presets_count;i++)
	{
		
		printf("\n\n ID = %d", i);
		printf("\n LED: R = %d G = %d B = %d",
			presets[i].r,presets[i].g,presets[i].b);
		printf("\n Humidifier = %d\t Fan = %d\n\n", presets[0].humidifier,
			presets[0].fan);
	}
}

void add_profile()
{
	display_profiles();

	if(stored_presets_count >= MAX_PRESETS)
		printf("\n\n Cannot add more presets.\n");

	int i,r,g,b,f,h;

	printf("\nID = ", stored_presets_count);
	printf("\n Value of Red: ");
	scanf("%d",&r);

	printf("\n Value of Green: ");
	scanf("%d",&g);	

	printf("\n Value of Blue: ");
	scanf("%d",&b);	

	printf("\n Humidifier: ");
	scanf("%d",&h);	

	printf("\n Fan: ");
	scanf("%d",&f);	

	for(i = 0;i<4;i++)
	{
		presets[stored_presets_count].r = r;
		presets[stored_presets_count].g = g;
		presets[stored_presets_count].b = b;
	}
	presets[stored_presets_count].humidifier = h;
	presets[stored_presets_count].fan = f;
	stored_presets_count++;
}

void menu()
{
	int choice;
    printf("1] Add a Profile\n");
    printf("2] Display Existing Profiles\n");
    printf("3] Update Profile\n");
    scanf("%d",&choice);
    if(choice <1 || choice > 3)
    	menu();
    switch(choice)
    {
    	case 1: add_profile();
    			break;
    	case 2: display_profiles();
    			break;
    	default: break;
    }
    manual_input = 0;

}

void install_sig_handlers()
{
	if(signal(SIGTSTP, exit_handler) == SIG_ERR)
	{
		printf("Couldn't initialize the signal handler. The program will not terminate in a stable manner\n");
	}

}


void write_firefly_data_to_file(struct node_packet *b_packet)
{
	FILE *fd_firefly_data = fopen(FIREFLY_DATA_FILE,"w");
	fwrite(b_packet, sizeof(struct node_packet), 1, fd_firefly_data);
	fclose(fd_firefly_data);	
}

int main()
{

	int i, min;
	//init_arduino();
	init_firefly();
	install_sig_handlers();
	//read_presets_from_file();
	//write_to_arduino(presets+3);


	while(exit_condition == 0)
	{
		printPacketToFile();
		write_firefly_data_to_file(&packet);
	} 
	
	return 1;
}