#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include <time.h>
#include"csapp.h"

#define clear() printf("\033[H\033[J")

static int exit_status;

void exit_handler(int signal)
{
	exit_status = 1;
}

int main()
{
	char buffer[999],c;
	int fd_log = Open("../log.txt", O_RDONLY,0);
	signal(SIGTSTP,exit_handler);
	while(exit_status == 0)
	{
		if(Read(fd_log, &c, 1)>0)
		{
			if(c == '\r')
				clear();
			printf("%c",c);
		}
			
	}
	
	return 0;
	
}