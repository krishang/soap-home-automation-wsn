String parse_curr(String s, char delimiter)
{
  int del_pos=s.indexOf(delimiter);

  String s_0=s.substring(0,del_pos);
  return s_0;
}

String parse_next(String s, char delimiter)
{
  int del_pos=s.indexOf(delimiter);

  String s_0=s.substring(del_pos+1);
  return s_0;
}

int serial_parse(String line)
{
  int k=0;

  for(int i=0;i<line.length();i++)
  {
    if(line[i]==':')
    {
      k++;
    }
  }
  if(k!=5)
  {
    return -1;    
  }

  
  String header=parse_curr(line,':');
  if(header!="AU")
  {
    return -1;
  }
    
  String s_1=parse_next(line,':');
  
    r[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(r[i]);
    g[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(g[i]);
    b[0]=parse_curr(s_1,':').toInt();
    s_1=parse_next(s_1,':');
    //Serial.println(b[i]);
  for(int i = 1; i<4;i++)
  {
    r[i] = r[0];
    g[i] = g[0];
    b[i] = b[0];      
  }

  fan=parse_curr(s_1,':').toInt();
  //Serial.println(fan);
  s_1=parse_next(s_1,':');
  humid=parse_next(s_1,':').toInt();
  //Serial.println(humid);
  return 0;
}

