#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>


#define FAN 0
#define HUMIDIFIER 1
#define FAN_HUMIDIFER 2
#define ON 1
#define OFF 0


int r[4],g[4],b[4];
int fan=0,humid=0;

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
#if defined(ARDUINO_ARCH_SAMD)  
// for Zero, output on USB Serial console, remove line below if using programming port to program the Zero!
   #define Serial SerialUSB
#endif


//Fan Pins
int Motor1_1=2;
int Motor1_2=3;
int Motor2_1=4;
int Motor2_2=5;
int pwm_1=9;
int pwm_2=10;
int speed=255;
//Fan Pins

//FAN/HUMIDIFIER PINS
int on_off=7;
int mode=8;
//FAN/HUMIDIFIER PINS

String line;
void setup() {
//Serial Data from Pi
  Serial.begin(115200);
  //Serial.setTimeout(100);
//FAN SETUP
  pinMode(Motor1_1, OUTPUT);
  pinMode(Motor1_2, OUTPUT);
  pinMode(Motor2_1, OUTPUT);
  pinMode(Motor2_2, OUTPUT); 
  pinMode(pwm_1, OUTPUT);
  pinMode(pwm_2, OUTPUT);
//FAN SETUP

//LEDs SETUP

  pwm.begin();
  pwm.setPWMFreq(1600);  // This is the maximum PWM frequency
  #ifdef TWBR    
  uint8_t twbrbackup = TWBR;
  TWBR = 12; // upgrade to 400KHz!
  #endif
//LEDs SETUP

//FAN/HUMIDIFER SETUP
  pinMode(on_off, OUTPUT);
  pinMode(mode, OUTPUT);
  digitalWrite(mode,HIGH);
//FAN/HUMIDIFIER SETUP
pinMode(13,OUTPUT);
}
int serial_parse(String line);
int i=0;
int mode_count=-1;

void loop() {

  int x=1;
  if(Serial.available()>0 )
  {
    
    String line= Serial.readStringUntil('k');
    x= serial_parse(line);
    
  }

  //Test rgb led
  for(int i=0;i<4;i++)
    {
      led_custom(r[i],g[i],b[i],i);
    }

  //Test Fan
  //rotate_right(1,speed);

  //Test Humidifer/Fan
  int mode=0;
  if(fan==0 && humid==0) 
    fan_humid_off();
  else
  {
    if(fan==1 && humid==0) mode=0;
    if(fan==0 && humid==1) mode=1;
    if(fan==1 && humid==1) mode=2;
    fan_humid_on(mode);
  }
  //delay(10000);
  //fan_humid_off();
  if(x==0)
      Serial.println("ACK");
  else if(x==-1)
    Serial.println("INB");
    
  
}
